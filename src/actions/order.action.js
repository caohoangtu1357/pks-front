import {
    GET_ORDER_ALL_SUCCESS
} from "../config/ActionTypes/Order";

export function getOrderAllSuccess(data){
    return {type:GET_ORDER_ALL_SUCCESS,data:data}
}