import {
    LOGIN_SUCCESS,
    LOGOUT_SUCCESS
} from "../config/ActionTypes/Auth";

export function logginSuccess(data) {
    return { type: LOGIN_SUCCESS, data: data }
}

export function loggoutSuccess() {
    return { type: LOGOUT_SUCCESS }
}