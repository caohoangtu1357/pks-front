import {
    LOAD_MORE_POST_SUCCESS
} from "../config/ActionTypes/Post";

export function loadMorePostSuccess(data){
    return {type:LOAD_MORE_POST_SUCCESS, data}
}