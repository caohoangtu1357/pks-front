import {
    GET_RECHARGE_SUCCESS
} from "../config/ActionTypes/Recharge";

export function getRechargeSuccess(data){
    return {type:GET_RECHARGE_SUCCESS,data:data}
}