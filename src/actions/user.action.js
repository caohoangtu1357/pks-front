import {
    GET_INFO_SUCCESS,
    CHANGE_PASSWORD_SUCCESS,
    UPDATE_FB_ID_SUCCESS
} from "../config/ActionTypes/User";

export function getInfoSuccess(data) {
    return { type: GET_INFO_SUCCESS, data: data }
}

export function getInfoFail() {
    return { type: GET_INFO_SUCCESS}
}

export function changePasswordSuccess(){
    return { type: CHANGE_PASSWORD_SUCCESS}
}

export function updateFbIdSuccess(){
    return {type:UPDATE_FB_ID_SUCCESS}
}