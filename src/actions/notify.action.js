import {
    LOAD_NOTIFY_SUCCESS
} from "../config/ActionTypes/Notify";

export function getNotifySuccess(data){
    return {type:LOAD_NOTIFY_SUCCESS, data}
}