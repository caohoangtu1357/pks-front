import {SHOW_ALERT, TURN_OFF_ALERT} from "../config/ActionTypes/Alert";

export function showAlert(data){
    return {type:SHOW_ALERT,data:data}
}

export function turnOffAlert(){
    return {type:TURN_OFF_ALERT}
}