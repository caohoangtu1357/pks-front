import {
    GET_BUFF_SHARE_ALL_SUCCESS,
    CREATE_BUFF_SHARE_SUCCESS
} from "../config/ActionTypes/BuffShare";

export function getBuffShareAllSuccess(data){
    return {type:GET_BUFF_SHARE_ALL_SUCCESS,data:data}
}

export function createBuffShareSuccess(){
    return {type:CREATE_BUFF_SHARE_SUCCESS}
}