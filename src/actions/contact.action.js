import {
    GET_CONTACT_SUCCESS, 
    DELETE_CONTACT_SUCCESS,
    CREATE_CONTACT_SUCCESS,
    GET_CONTACT_ALL_SUCCESS,
    GET_CONTACT_DETAIL_SUCCESS,
    UPDATE_CONTACT_SUCCESS,
    STATISTIC_CONTACT_SUCCESS
} from "../config/ActionTypes/Contact";

export function createContactSuccess(data){
    return {type:CREATE_CONTACT_SUCCESS,data:data}
}

export function deleteContactSuccess(){
    return {type:DELETE_CONTACT_SUCCESS}
}

export function getContactSuccess(){
    return {type:GET_CONTACT_SUCCESS}
}

export function getContactAllSuccess(data){
    return {type:GET_CONTACT_ALL_SUCCESS,data}
}

export function getDetailContactSuccess(data){
    return {type:GET_CONTACT_DETAIL_SUCCESS,data}
}

export function updateContactSuccess(){
    return {type:UPDATE_CONTACT_SUCCESS}
}

export function statiscticContactSuccess(data){
    return {type:STATISTIC_CONTACT_SUCCESS,data}
}