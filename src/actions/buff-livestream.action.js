import {
    GET_BUFF_LIVE_STREAM_ALL_SUCCESS,
    CREATE_BUFF_LIVE_STREAM_SUCCESS
} from "../config/ActionTypes/BuffLiveStream";

export function getBuffLiveStreamAllSuccess(data){
    return {type:GET_BUFF_LIVE_STREAM_ALL_SUCCESS,data:data}
}

export function createBuffLiveStreamSuccess(){
    return {type:CREATE_BUFF_LIVE_STREAM_SUCCESS}
}