import {
    GET_BUFF_LIKE_ALL_SUCCESS,
    CREATE_BUFF_LIKE_SUCCESS
} from "../config/ActionTypes/BuffLike";

export function getBuffLikeAllSuccess(data){
    return {type:GET_BUFF_LIKE_ALL_SUCCESS,data:data}
}

export function createBuffLikeSuccess(){
    return {type:CREATE_BUFF_LIKE_SUCCESS}
}