import {
    GET_BUFF_COMMENT_ALL_SUCCESS,
    CREATE_BUFF_COMMENT_SUCCESS
} from "../config/ActionTypes/BuffComment";

export function getBuffCommentAllSuccess(data){
    return {type:GET_BUFF_COMMENT_ALL_SUCCESS,data:data}
}

export function createBuffCommentSuccess(){
    return {type:CREATE_BUFF_COMMENT_SUCCESS}
}