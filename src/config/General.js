const API_URL = process.env.REACT_APP_BASE_URL;

const SERVER_CONTACT_IMAGE = process.env.REACT_APP_SERVER_CONTACT_IMAGE;

function extractError(error) {
    if (error.msg) {
        return error.msg;
    }
    var errors = error.errors;
    for (const property in errors) {
        return errors[property];
    }
    return "Lỗi hệ thống.";
}

function getContactImageUrl(imagePath) {
    if (typeof imagePath == "string") {
        return SERVER_CONTACT_IMAGE + imagePath.replace("public", "storage")
    }
    return "";
}

function processIdPost(props, idPost) {
    var id = idPost;
    var result = null;
    var post_id = id['match'](/(.*)\/posts\/([0-9]{8,})/);
    var photo_id = id['match'](/(.*)\/photo.php\?fbid=([0-9]{8,})/);
    var photo_id1 = id['match'](/(.*)\/photo\?fbid=([0-9]{8,})/);
    var photo_id2 = id['match'](/(.*)\/photo\/\?fbid=([0-9]{8,})/);
    var video_id = id['match'](/(.*)\/video.php\?v=([0-9]{8,})/);
    var story_id = id['match'](/(.*)\/story.php\?story_fbid=([0-9]{8,})/);
    var link_id = id['match'](/(.*)\/permalink.php\?story_fbid=([0-9]{8,})/);
    var other_id = id['match'](/(.*)\/([0-9]{8,})/);
    var comment_id = id['match'](/(.*)comment_id=([0-9]{8,})/);
    if (post_id) {
        result = post_id[2]
    } else {
        if (photo_id) {
            result = photo_id[2]
        } else {
            if (photo_id1) {
                result = photo_id1[2]
            } else {
                if (photo_id2) {
                    result = photo_id2[2]
                } else {
                    if (video_id) {
                        result = video_id[2]
                    } else {
                        if (story_id) {
                            result = story_id[2]
                        } else {
                            if (link_id) {
                                result = link_id[2]
                            } else {
                                if (other_id) {
                                    result = other_id[2]
                                }
                            }
                        }
                    }
                }
            }
        }
    };
    if (comment_id) {
        result += '_' + comment_id[2]
    };

    if (result == null) {
        var regex = /^[0-9]+$/;
        if (id.match(regex)) {
            result = id;

        }
        else {
            props.showAlert("Link bài viết bạn nhập không hợp lệ", "danger");
            return false;
        }
    }
    return result;
}

function convertNumberToK(number) {
    var result = Math.floor(number / 1000);
    if (result > 0) {
        return result + "K";
    }
    return number;
}

function numberWithDot(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}


export {
    API_URL,
    SERVER_CONTACT_IMAGE,
    extractError,
    getContactImageUrl,
    processIdPost,
    convertNumberToK,
    numberWithDot
}