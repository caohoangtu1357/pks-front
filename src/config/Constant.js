const contactProblems = [
    {value:"",label:"Chọn vấn đề cần hỗ trợ.",option:"selected disabled"},
    {value:"Buff like bài viết facebook",label:"Buff like bài viết facebook"},
    {value:"Buff Cmt bài viết facebook",label:"Buff Cmt bài viết facebook"},
    {value:"Buff share bài viết facebook",label:"Buff share bài viết facebook"},
    {value:"Buff livestream facebook",label:"Buff livestream facebook"}
]

const CONTACT_STATUS = [
    {value:0, label:"Chờ hỗ trợ"},
    {value:1, label:"Đang hổ trợ"},
    {value:2, label:"Đã hỗ trợ"}
]

const TIME_LIVESTREAM=[
    {value:30, label:"30 phút"},
    {value:45, label:"45 phút"},
    {value:60, label:"60 phút"},
    {value:90, label:"90 phút"},
    {value:120, label:"120 phút"},
    {value:150, label:"150 phút"},
    {value:180, label:"180 phút"},
    {value:210, label:"210 phút"},
    {value:240, label:"240 phút"},
]

const PRICE = {
    'PRICE_VIEW':process.env.REACT_APP_PRICE_VIEW,
    'PRICE_COMMENT':process.env.REACT_APP_PRICE_COMMENT,
    'PRICE_REACTION':process.env.REACT_APP_PRICE_REACTION
}

const URL_STATIC_POST = process.env.REACT_APP_URL_STATIC_POST

export {
    contactProblems,
    CONTACT_STATUS,
    TIME_LIVESTREAM,
    PRICE,
    URL_STATIC_POST
}