import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL, extractError } from "../../config/General";
import axios from "axios";
import {
    createBuffShareSuccess,
} from "../../actions/buff-share.action";
import { showAlert, turnOffAlert } from "../../actions/alert.action.js";
import { CREATE_BUFF_SHARE } from "../../config/ActionTypes/BuffShare";
import { ALERT } from "../../config/ActionTypes/Alert";
import {GET_INFO} from "../../config/ActionTypes/User";

async function createBuffShareAsync(attributes) {
    try {
        return await axios.post(API_URL + "buff-share/create", attributes)
    } catch (error) {
        throw error.response.data;
    }
}

function* createBuffShare(data) {
    try {
        yield call(createBuffShareAsync, data.attributes);
        yield put(createBuffShareSuccess());
        yield put({type:ALERT,info:{content:"Thanh toán thành công",variant:"success"}});
        yield put({type:GET_INFO});
    } catch (error) {
        yield put({type:ALERT,info:{content:extractError(error),variant:"danger"}})
    }
}

export function* watchCreateBuffShare() {
    yield takeLatest(CREATE_BUFF_SHARE, createBuffShare);
}