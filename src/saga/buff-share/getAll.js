import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL, extractError } from "../../config/General";
import axios from "axios";
import {
    getBuffShareAllSuccess,
} from "../../actions/buff-share.action";
import { showAlert, turnOffAlert } from "../../actions/alert.action.js";
import { GET_BUFF_SHARE_ALL } from "../../config/ActionTypes/BuffShare";

async function getAllBuffShareAsync(filters) {
    try {
        return await axios.post(API_URL + "buff-share/data-table", filters)
    } catch (error) {
        throw error.response.data;
    }
}

function* getAllBuffShare(data) {
    try {
        data = yield call(getAllBuffShareAsync, data.filters);
        yield put(getBuffShareAllSuccess(data.data));
    } catch (error) {
        yield put(showAlert({
            content: extractError(error),
            variant: "danger"
        }));
    }
    yield delay(5000);
    yield put(turnOffAlert());
}

export function* watchGetAllBuffShare() {
    yield takeLatest(GET_BUFF_SHARE_ALL, getAllBuffShare);
}