import { all } from "redux-saga/effects";
import rootAuth from "./auth";
import rootUser from "./user";
import rootRecharge from "./recharge";
import rootContact from "./contact";
import rootOrder from "./order";
import rootBuffLike from "./buff-like";
import rootBuffComment from "./buff-comment";
import rootBuffLiveStream from "./buff-livestream";
import rootBuffShare from "./buff-share";
import rootAlert from "./alert";
import rootPost from "./post";
import rootNotify from "./notification";

export default function* rootSaga() {
  yield all([
    rootAuth(),
    rootUser(),
    rootRecharge(),
    rootContact(),
    rootOrder(),
    rootBuffLike(),
    rootBuffComment(),
    rootBuffLiveStream(),
    rootBuffShare(),
    rootAlert(),
    rootPost(),
    rootNotify()
  ]);
}
