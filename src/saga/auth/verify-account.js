import {takeLatest,call,put} from "redux-saga/effects";
import {API_URL} from "../../config/General";
import axios from "axios";
import {VERIFY_ACCOUNT} from "../../config/ActionTypes/Auth";
import {ALERT} from "../../config/ActionTypes/Alert";
import { extractError } from "../../config/General";

async function verifyAccountAsync(data){
    try {
        let res = await axios.post(API_URL + "verify-sms-token", {...data})
        return res.data;
    } catch (error) {
        throw error.response.data;
    }
}

function* verifyAccount(actionData){
    try{
        yield call(verifyAccountAsync,actionData.credentials);
        yield put({type:ALERT,info:{content:"Xác thực tài khoản thành công.",variant:"success"}});
        actionData.props.history.push("login");
    }catch(error){
        yield put({type:ALERT,info:{content:extractError(error),variant:"danger"}});
    }
}

export function* watchVerifyAccount(){
    yield takeLatest(VERIFY_ACCOUNT,verifyAccount);
}