import {takeLatest,call,put} from "redux-saga/effects";
import {API_URL} from "../../config/General";
import axios from "axios";
import {VERIFY_RESET_PASSWORD} from "../../config/ActionTypes/Auth";
import { ALERT} from "../../config/ActionTypes/Alert";
import { extractError } from "../../config/General";

async function verifyResetPasswordAsync(data){
    try {
        let res = await axios.post(API_URL + "verify-reset-password", {...data})
        return res.data;
    } catch (error) {
        throw error.response.data;
    }
}

function* verifyResetPassword(actionData){
    try{
        yield call(verifyResetPasswordAsync,actionData.credentials);
        yield put({type:ALERT,info:{content:"Đặt lại mật khẩu thành công.",variant:"success"}});
        actionData.props.history.push("login");
    }catch(error){
        yield put({type:ALERT,info:{content:extractError(error),variant:"danger"}});
    }
}

export function* watchVerifyResetPassword(){
    yield takeLatest(VERIFY_RESET_PASSWORD,verifyResetPassword);
}