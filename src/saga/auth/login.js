import {takeLatest,call,put} from "redux-saga/effects";
import {API_URL} from "../../config/General";
import axios from "axios";
import {LOGIN} from "../../config/ActionTypes/Auth";
import { GET_INFO } from "../../config/ActionTypes/User";
import {logginSuccess} from "../../actions/auth.action";
import { ALERT} from "../../config/ActionTypes/Alert";
import { extractError } from "../../config/General";

async function loginAsync(data){
    try {
        let res = await axios.post(API_URL + "login", {...data})
        return res.data;
    } catch (error) {
        throw error.response.data;
    }
}

function* login(actionData){
    try{
        let data = yield call(loginAsync,actionData.credentials);
        localStorage.setItem("auth-token",data['token_type']+" "+data["token"]);
        yield put(logginSuccess(data));
        yield put({type:GET_INFO});
    }catch(error){
        yield put({type:ALERT,info:{content:extractError(error),variant:"danger"}});
    }
}

export function* watchLogin(){
    yield takeLatest(LOGIN,login);
}