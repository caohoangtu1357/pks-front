import {takeLatest,call,put} from "redux-saga/effects";
import {API_URL} from "../../config/General";
import axios from "axios";
import {RESET_PASSWORD} from "../../config/ActionTypes/Auth";
import { ALERT} from "../../config/ActionTypes/Alert";
import { extractError } from "../../config/General";

async function resetPasswordAsync(data){
    try {
        let res = await axios.post(API_URL + "reset-password", {...data})
        return res.data;
    } catch (error) {
        throw error.response.data;
    }
}

function* resetPassword(actionData){
    try{
        yield call(resetPasswordAsync,actionData.credentials);
        actionData.props.history.push("verify-reset-password");
    }catch(error){
        yield put({type:ALERT,info:{content:extractError(error),variant:"danger"}});
    }
}

export function* watchResetPassword(){
    yield takeLatest(RESET_PASSWORD,resetPassword);
}