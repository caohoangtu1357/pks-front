import {takeLatest,call,put} from "redux-saga/effects";
import {API_URL} from "../../config/General";
import axios from "axios";
import { REGISTER } from "../../config/ActionTypes/Auth";
import { ALERT } from "../../config/ActionTypes/Alert";
import { extractError } from "../../config/General";

async function registerAsync(data){
    try {
        return await axios.post(API_URL + "register", {...data})
    } catch (error) {
        throw error.response.data;
    }
}

function* register(data){
    try{
        yield call(registerAsync,data.credentials);
        data.props.history.push("/verify-account");
    }catch(error){
        yield put({type:ALERT,info:{content:extractError(error),variant:"danger"}});
    }
}

export function* watchRegister(){
    yield takeLatest(REGISTER,register);
}