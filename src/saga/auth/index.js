import {all} from "redux-saga/effects";
import {watchLogin} from "./login";
import { watchLogout } from "./logout";
import {watchRegister} from "./register";
import { watchVerifyAccount } from "./verify-account";
import { watchResetPassword } from "./reset-password";
import { watchVerifyResetPassword } from "./verify-reset-password";

export default function* rootAuth(){
    yield all([
        watchLogin(),
        watchLogout(),
        watchRegister(),
        watchVerifyAccount(),
        watchResetPassword(),
        watchVerifyResetPassword()
    ])
}