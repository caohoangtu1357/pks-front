import {all} from "redux-saga/effects";
import {watchLoadMorePost} from "./load-post";

export default function* rootPost(){
    yield all([
        watchLoadMorePost()
    ])
}