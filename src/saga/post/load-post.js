import axios from "axios";
import {
    call,takeLatest,put
} from "redux-saga/effects";
import { LOAD_MORE_POST } from "../../config/ActionTypes/Post";
import { ALERT } from "../../config/ActionTypes/Alert";
import {API_URL, extractError} from "../../config/General";
import {loadMorePostSuccess} from "../../actions/post.action";

function loadMorePostAsync(filters){
    return axios.post(API_URL+"post/ajax-load-more",filters).then(res=>{
        return Promise.resolve(res.data);
    }).catch(error=>{
        return Promise.reject(error);
    });
}

function* loadMorePost(data){
    try{
        let post = yield call(loadMorePostAsync,data.filters);
        yield put(loadMorePostSuccess(post));
    }catch(error){
        yield put({type:ALERT,info:{content:extractError(error),variant:"danger"}});
    }
}

export function* watchLoadMorePost(){
    yield takeLatest(LOAD_MORE_POST,loadMorePost);
}