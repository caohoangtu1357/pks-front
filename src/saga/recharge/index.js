import {all} from "redux-saga/effects";
import {watchGetInfo} from "./get-info";

export default function* rootRecharge(){
    yield all([
        watchGetInfo()
    ])
}