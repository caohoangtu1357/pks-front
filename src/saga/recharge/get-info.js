import axios from "axios";
import {
    call,takeLatest,put
} from "redux-saga/effects";
import { GET_RECHARGE } from "../../config/ActionTypes/Recharge";
import {API_URL} from "../../config/General";
import {getRechargeSuccess} from "../../actions/recharge.action";

function getInfoAsync(){
    return axios.get(API_URL+"recharge/get-info").then(res=>{
        return Promise.resolve(res.data);
    }).catch(error=>{
        return Promise.reject(error);
    });
}

function* getInfo(){
    try{
        let data = yield call(getInfoAsync);
        yield put(getRechargeSuccess(data));
    }catch(error){
        //call alert
    }
}

export function* watchGetInfo(){
    yield takeLatest(GET_RECHARGE,getInfo);
}