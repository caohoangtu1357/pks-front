import {all} from "redux-saga/effects";
import { watchGetAllBuffLiveStream } from "./getAll";
import {watchCreateBuffLiveStream} from "./create";

export default function* rootBuffLiveStream(){
    yield all([
        watchGetAllBuffLiveStream(),
        watchCreateBuffLiveStream()
    ])
}