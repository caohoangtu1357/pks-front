import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL, extractError } from "../../config/General";
import axios from "axios";
import {
    createBuffLiveStreamSuccess,
} from "../../actions/buff-livestream.action";
import { CREATE_BUFF_LIVE_STREAM } from "../../config/ActionTypes/BuffLiveStream";
import {GET_INFO} from "../../config/ActionTypes/User";
import {ALERT} from "../../config/ActionTypes/Alert";

async function createBuffLiveStreamAsync(attributes) {
    try {
        return await axios.post(API_URL + "buff-livestream/create", attributes)
    } catch (error) {
        throw error.response.data;
    }
}

function* createBuffLiveStream(data) {
    try {
        yield call(createBuffLiveStreamAsync, data.attributes);
        yield put(createBuffLiveStreamSuccess());
        yield put({type:ALERT,info:{content:"Thanh toán thành công",variant:"success"}});
        yield put({type:GET_INFO});
    } catch (error) {
        yield put({type:ALERT,info:{content:extractError(error),variant:"danger"}});
    }
}

export function* watchCreateBuffLiveStream() {
    yield takeLatest(CREATE_BUFF_LIVE_STREAM, createBuffLiveStream);
}