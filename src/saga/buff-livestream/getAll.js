import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL, extractError } from "../../config/General";
import axios from "axios";
import {
    getBuffLiveStreamAllSuccess,
} from "../../actions/buff-livestream.action";
import { showAlert, turnOffAlert } from "../../actions/alert.action.js";
import { GET_BUFF_LIVE_STREAM_ALL } from "../../config/ActionTypes/BuffLiveStream";

async function getAllBuffLiveStreamAsync(filters) {
    try {
        return await axios.post(API_URL + "buff-livestream/data-table", filters)
    } catch (error) {
        throw error.response.data;
    }
}

function* getAllBuffLiveStream(data) {
    try {
        data = yield call(getAllBuffLiveStreamAsync, data.filters);
        yield put(getBuffLiveStreamAllSuccess(data.data));
    } catch (error) {
        yield put(showAlert({
            content: extractError(error),
            variant: "danger"
        }));
    }
    yield delay(5000);
    yield put(turnOffAlert());
}

export function* watchGetAllBuffLiveStream() {
    yield takeLatest(GET_BUFF_LIVE_STREAM_ALL, getAllBuffLiveStream);
}