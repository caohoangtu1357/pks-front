import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL, extractError } from "../../config/General";
import axios from "axios";
import {
    deleteContactSuccess,
} from "../../actions/contact.action";
import { showAlert, turnOffAlert } from "../../actions/alert.action.js";
import { DELETE_CONTACT, STATISTIC_CONTACT } from "../../config/ActionTypes/Contact";

async function deleteContactAsync(id) {
    try {
        return await axios.post(API_URL + "contact/deleted", {id:id})
    } catch (error) {
        throw error.response.data;
    }
}

function* deleteContact(data) {
    try {
        yield call(deleteContactAsync, data.id);
        yield put(deleteContactSuccess());
        yield put(showAlert({
            content: "Xóa liên hệ hỗ trợ thành công",
            variant: "success"
        }));
        yield put({type:STATISTIC_CONTACT});
    } catch (error) {
        yield put(showAlert({
            content: extractError(error),
            variant: "danger"
        }));
    }
    yield delay(5000);
    yield put(turnOffAlert());
}

export function* watchDeleteContact() {
    yield takeLatest(DELETE_CONTACT, deleteContact);
}