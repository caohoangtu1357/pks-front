import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL, extractError } from "../../config/General";
import axios from "axios";
import {
    createContactSuccess,
} from "../../actions/contact.action";
import { showAlert, turnOffAlert } from "../../actions/alert.action.js";
import { CREATE_CONTACT } from "../../config/ActionTypes/Contact";
import FormData from 'form-data'

async function createContactAsync(contactInfo) {
    try {
        let dataForm = new FormData();
        dataForm.append('contact_id', contactInfo.contact_id);
        dataForm.append('problem', contactInfo.problem);
        dataForm.append('title', contactInfo.title);
        dataForm.append('description', contactInfo.description);
        dataForm.append('image', contactInfo.image);

        return await axios.post(API_URL + "contact/create", dataForm)
    } catch (error) {
        throw error.response.data;
    }
}

function* createContact(data) {
    try {
        yield call(createContactAsync, data.contactInfo);
        yield put(createContactSuccess());
        yield put(showAlert({
            content: "Tạo liên hệ hỗ trợ thành công",
            variant: "success"
        }));
        data.props.history.push("/contact");
    } catch (error) {
        yield put(showAlert({
            content: extractError(error),
            variant: "danger"
        }));
    }
    yield delay(5000);
    yield put(turnOffAlert());
}

export function* watchCreateContact() {
    yield takeLatest(CREATE_CONTACT, createContact);
}