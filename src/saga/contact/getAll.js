import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL, extractError } from "../../config/General";
import axios from "axios";
import {
    getContactAllSuccess,
} from "../../actions/contact.action";
import { showAlert, turnOffAlert } from "../../actions/alert.action.js";
import { GET_CONTACT_ALL } from "../../config/ActionTypes/Contact";

async function getAllContactAsync(filters) {
    try {
        return await axios.post(API_URL + "contact/data-table", filters)
    } catch (error) {
        throw error.response.data;
    }
}

function* getAllContact(data) {
    try {
        data = yield call(getAllContactAsync, data.filters);
        yield put(getContactAllSuccess(data.data));
    } catch (error) {
        yield put(showAlert({
            content: extractError(error),
            variant: "danger"
        }));
    }
    yield delay(5000);
    yield put(turnOffAlert());
}

export function* watchGetAllContact() {
    yield takeLatest(GET_CONTACT_ALL, getAllContact);
}