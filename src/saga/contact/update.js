import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL, extractError } from "../../config/General";
import axios from "axios";
import {
    updateContactSuccess
} from "../../actions/contact.action";
import { showAlert, turnOffAlert } from "../../actions/alert.action.js";
import { UPDATE_CONTACT } from "../../config/ActionTypes/Contact";
import FormData from 'form-data'

async function updateContactAsync(contactInfo,id) {
    try {
        let dataForm = new FormData();
        dataForm.append('contact_id', contactInfo.contact_id);
        dataForm.append('problem', contactInfo.problem);
        dataForm.append('title', contactInfo.title);
        dataForm.append('description', contactInfo.description);
        if(contactInfo.image){
            dataForm.append('image', contactInfo.image);
        }
        return await axios.post(API_URL + "contact/update/"+id, dataForm)
    } catch (error) {
        throw error.response.data;
    }
}

function* updateContact(data) {
    try {
        yield call(updateContactAsync, data.contactInfo,data.id);
        yield put(updateContactSuccess());
        yield put(showAlert({
            content: "Cập nhật liên hệ hỗ trợ thành công",
            variant: "success"
        }));
        data.props.history.push("/contact");
    } catch (error) {
        yield put(showAlert({
            content: extractError(error),
            variant: "danger"
        }));
    }
    yield delay(5000);
    yield put(turnOffAlert());
}

export function* watchUpdateContact() {
    yield takeLatest(UPDATE_CONTACT, updateContact);
}