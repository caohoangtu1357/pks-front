import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL, extractError } from "../../config/General";
import axios from "axios";
import {
    getDetailContactSuccess,
} from "../../actions/contact.action";
import { showAlert, turnOffAlert } from "../../actions/alert.action.js";
import { GET_CONTACT_DETAIL } from "../../config/ActionTypes/Contact";

async function getDetailContactAsync(id) {
    try {
        return await axios.get(API_URL + "contact/detail/"+id)
    } catch (error) {
        throw error.response.data;
    }
}

function* getDetailContact(id) {
    try {
        var data = yield call(getDetailContactAsync, id.id);
        yield put(getDetailContactSuccess(data.data));
    } catch (error) {
        yield put(showAlert({
            content: extractError(error),
            variant: "danger"
        }));
    }
    yield delay(5000);
    yield put(turnOffAlert());
}

export function* watchGetDetailContact() {
    yield takeLatest(GET_CONTACT_DETAIL, getDetailContact);
}