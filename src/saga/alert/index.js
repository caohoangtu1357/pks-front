import {all} from "redux-saga/effects";
import { watchAlert } from "./show";

export default function* rootAlert(){
    yield all([
        watchAlert()
    ])
}