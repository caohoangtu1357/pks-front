import {takeLatest,call,put} from "redux-saga/effects";
import {API_URL} from "../../config/General";
import axios from "axios";
import {GET_INFO} from "../../config/ActionTypes/User";
import {
    getInfoFail,
    getInfoSuccess
} from "../../actions/user.action";

function getInfoAsync(){
    return axios.get(API_URL+"user/user-info").then(res=>{
        return Promise.resolve(res.data);
    }).catch(error=>{
        return Promise.reject(error);
    });
}

function* getInfo(){
    try{
        let data = yield call(getInfoAsync);
        yield put(getInfoSuccess(data));
    }catch(error){
        yield put(getInfoFail());
    }
}

export function* watchGetInfo(){
    yield takeLatest(GET_INFO,getInfo);
}