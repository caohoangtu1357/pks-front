import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL, extractError } from "../../config/General";
import axios from "axios";
import { UPDATE_FB_ID, GET_INFO } from "../../config/ActionTypes/User";
import {
    updateFbIdSuccess,
} from "../../actions/user.action";
import { showAlert, turnOffAlert } from "../../actions/alert.action.js";

async function updateFbIdAsync(fbID) {
    try {
        return await axios.post(API_URL + "user/update-fb-id", fbID)
    } catch (error) {
        throw error.response.data;
    }
}

function* updateFbId(fbID) {
    try {
        yield call(updateFbIdAsync, fbID.fbId);
        yield put(updateFbIdSuccess());
        yield put({ type: GET_INFO });
        yield put(showAlert({
            content: "Cập nhật FB ID thành công",
            variant: "success"
        }));
    } catch (error) {
        yield put(showAlert({
            content: extractError(error),
            variant: "danger"
        }));
    }
    yield delay(5000);
    yield put(turnOffAlert());
}

export function* watchUpdateFbId() {
    yield takeLatest(UPDATE_FB_ID, updateFbId);
}