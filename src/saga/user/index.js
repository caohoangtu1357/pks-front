import {all} from "redux-saga/effects";
import { watchChangePassword } from "./change-password";
import {watchGetInfo} from "./get-info";
import { watchUpdateFbId } from "./update-fb-id";

export default function* rootUser(){
    yield all([
        watchGetInfo(),
        watchChangePassword(),
        watchUpdateFbId()
    ])
}