import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL, extractError } from "../../config/General";
import axios from "axios";
import { CHANGE_PASSWORD, GET_INFO } from "../../config/ActionTypes/User";
import {
    changePasswordSuccess,
} from "../../actions/user.action";
import { showAlert, turnOffAlert } from "../../actions/alert.action.js";

async function changePasswordAsync(passwordInfo) {
    try {
        return await axios.post(API_URL + "user/change-password", passwordInfo.passwordInfo)
    } catch (error) {
        throw error.response.data;
    }
}

function* changePassword(passwordInfo) {
    try {
        yield call(changePasswordAsync, passwordInfo);
        yield put(changePasswordSuccess());
        yield put({ type: GET_INFO });
        yield put(showAlert({
            content: "Cập nhật mật khẩu thành công",
            variant: "success"
        }));
    } catch (error) {
        yield put(showAlert({
            content: extractError(error),
            variant: "danger"
        }));
    }
    yield delay(5000);
    yield put(turnOffAlert());
}

export function* watchChangePassword() {
    yield takeLatest(CHANGE_PASSWORD, changePassword);
}