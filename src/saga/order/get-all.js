import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL, extractError } from "../../config/General";
import axios from "axios";
import {
    getOrderAllSuccess,
} from "../../actions/order.action";
import { showAlert, turnOffAlert } from "../../actions/alert.action.js";
import { GET_ORDER_ALL } from "../../config/ActionTypes/Order";

async function getAllOrderAsync(filters) {
    try {
        return await axios.post(API_URL + "order/data-table", filters)
    } catch (error) {
        throw error.response.data;
    }
}

function* getAllOrder(data) {
    try {
        data = yield call(getAllOrderAsync, data.filters);
        yield put(getOrderAllSuccess(data.data));
    } catch (error) {
        yield put(showAlert({
            content: extractError(error),
            variant: "danger"
        }));
    }
    yield delay(5000);
    yield put(turnOffAlert());
}

export function* watchGetAllOrder() {
    yield takeLatest(GET_ORDER_ALL, getAllOrder);
}