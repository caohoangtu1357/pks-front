import {all} from "redux-saga/effects";
import { watchGetAllOrder } from "./get-all";
export default function* rootContact(){
    yield all([
        watchGetAllOrder()
    ])
}