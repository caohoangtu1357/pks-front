import { takeLatest, call, put } from "redux-saga/effects";
import { API_URL, extractError } from "../../config/General";
import axios from "axios";
import {
    createBuffCommentSuccess,
} from "../../actions/buff-comment.action";
import { CREATE_BUFF_COMMENT } from "../../config/ActionTypes/BuffComment";
import { ALERT } from "../../config/ActionTypes/Alert";
import {GET_INFO} from "../../config/ActionTypes/User";

async function createBuffCommentAsync(attributes) {
    try {
        return await axios.post(API_URL + "buff-comment/create", attributes)
    } catch (error) {
        throw error.response.data;
    }
}

function* createBuffComment(data) {
    try {
        yield call(createBuffCommentAsync, data.attributes);
        yield put(createBuffCommentSuccess());
        yield put({type:ALERT,info:{content:"Thanh toán thành công",variant: "success"}});
        yield put({type:GET_INFO});
    } catch (error) {
        yield put({type:ALERT,info:{content:extractError(error),variant: "danger"}});
    }
}

export function* watchCreateBuffComment() {
    yield takeLatest(CREATE_BUFF_COMMENT, createBuffComment);
}