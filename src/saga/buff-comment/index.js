import { all } from "redux-saga/effects";
import { watchGetAllBuffComment } from "./getAll";
import { watchCreateBuffComment } from "./create";

export default function* rootBuffComment() {
    yield all([
        watchGetAllBuffComment(),
        watchCreateBuffComment()
    ])
}