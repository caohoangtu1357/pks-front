import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL, extractError } from "../../config/General";
import axios from "axios";
import {
    getBuffCommentAllSuccess,
} from "../../actions/buff-comment.action";
import { showAlert, turnOffAlert } from "../../actions/alert.action.js";
import { GET_BUFF_COMMENT_ALL } from "../../config/ActionTypes/BuffComment";

async function getAllBuffCommentAsync(filters) {
    try {
        return await axios.post(API_URL + "buff-comment/data-table", filters)
    } catch (error) {
        throw error.response.data;
    }
}

function* getAllBuffComment(data) {
    try {
        data = yield call(getAllBuffCommentAsync, data.filters);
        yield put(getBuffCommentAllSuccess(data.data));
    } catch (error) {
        yield put(showAlert({
            content: extractError(error),
            variant: "danger"
        }));
    }
    yield delay(5000);
    yield put(turnOffAlert());
}

export function* watchGetAllBuffComment() {
    yield takeLatest(GET_BUFF_COMMENT_ALL, getAllBuffComment);
}