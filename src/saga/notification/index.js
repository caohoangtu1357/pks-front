import {all} from "redux-saga/effects";
import { watchGetNotify } from "./get-all";
export default function* rootNotify(){
    yield all([
        watchGetNotify()
    ])
}