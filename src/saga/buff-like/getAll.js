import { takeLatest, call, put, delay } from "redux-saga/effects";
import { API_URL, extractError } from "../../config/General";
import axios from "axios";
import {
    getBuffLikeAllSuccess,
} from "../../actions/buff-like.action";
import { showAlert, turnOffAlert } from "../../actions/alert.action.js";
import { GET_BUFF_LIKE_ALL } from "../../config/ActionTypes/BuffLike";

async function getAllBuffLikeAsync(filters) {
    try {
        return await axios.post(API_URL + "buff-like/data-table", filters)
    } catch (error) {
        throw error.response.data;
    }
}

function* getAllBuffLike(data) {
    try {
        data = yield call(getAllBuffLikeAsync, data.filters);
        yield put(getBuffLikeAllSuccess(data.data));
    } catch (error) {
        yield put(showAlert({
            content: extractError(error),
            variant: "danger"
        }));
    }
    yield delay(5000);
    yield put(turnOffAlert());
}

export function* watchGetAllBuffLike() {
    yield takeLatest(GET_BUFF_LIKE_ALL, getAllBuffLike);
}