import { takeLatest, call, put } from "redux-saga/effects";
import { API_URL, extractError } from "../../config/General";
import axios from "axios";
import {
    createBuffLikeSuccess,
} from "../../actions/buff-like.action";
import { CREATE_BUFF_LIKE } from "../../config/ActionTypes/BuffLike";
import {GET_INFO} from "../../config/ActionTypes/User";
import {ALERT} from "../../config/ActionTypes/Alert";

async function createBuffLikeAsync(BuffLikeInfo) {
    try {
        return await axios.post(API_URL + "buff-like/create", BuffLikeInfo)
    } catch (error) {
        throw error.response.data;
    }
}

function* createBuffLike(data) {
    try {
        yield call(createBuffLikeAsync, data.attributes);
        yield put(createBuffLikeSuccess());
        yield put({type:ALERT,info:{content:"Thanh toán thành công",variant:"success"}});
        yield put({type:GET_INFO});
    } catch (error) {
        yield put({type:ALERT,info:{content:extractError(error),variant:"danger"}});
    }
}

export function* watchCreateBuffLike() {
    yield takeLatest(CREATE_BUFF_LIKE, createBuffLike);
}