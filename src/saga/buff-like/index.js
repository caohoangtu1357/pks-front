import {all} from "redux-saga/effects";
import { watchGetAllBuffLike } from "./getAll";
import {watchCreateBuffLike} from "./create";

export default function* rootBuffLike(){
    yield all([
        watchGetAllBuffLike(),
        watchCreateBuffLike()
    ])
}