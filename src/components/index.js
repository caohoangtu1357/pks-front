import Header from "./Header";
import SideBar from "./Sidebar";
import Footer from "./Footer";
import ScrollTop from "./ScrollTop";
import AuthSlider from "./AuthSlider";
import Loader from "./Loader";
import BuffUserCoin from "./Buff/UserCoin";
import Alert from "./Alert";
import ErrorInput from "./ErrorMsgInput";
export {
    Header,
    SideBar,
    Footer,
    ScrollTop,
    AuthSlider,
    Loader,
    BuffUserCoin,
    Alert,
    ErrorInput
}