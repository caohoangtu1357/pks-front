import {useEffect,useState} from "react";

function Select(props){
    
    useEffect(()=>{
        
    })

    return (
        <div className="form-group">
            <select name={props.name} className={props.className}>
                {props.data.map((item,index)=>{
                    if(item.value==props.selected){
                        return <option value={item.value} selected>{item.label}</option>
                    }
                    return <option value={item.value}>{item.label}</option>
                })}
            </select>
        </div>
    )
}