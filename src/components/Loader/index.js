import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';

function Loader(props) {

    useEffect(() => {

    }, []);

    return (
        <div className="load1">
            <div className="loader"></div>
        </div>
    )
}

function mapStateToProps(state) {
    return {
    }
}

function mapDispatchToProps(dispatch) {
    return {
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Loader);