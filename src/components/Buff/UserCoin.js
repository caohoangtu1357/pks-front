import { connect } from "react-redux";

function UserCoin(props) {
    return (
        <div className="panel">
            <div className="list-group bg-trans">
                <a href="#" className="list-group-item">
                    <div className="media-left pos-rel">
                        <img
                            className="img-circle img-xs"
                            src="img/profile-photos/2.png"
                            alt="Profile Picture"
                        />
                        <i className="badge badge-success badge-stat badge-icon pull-left" />
                    </div>
                    <div className="media-body">
                        <p className="mar-no">{props.user.full_name}</p>
                        <strong>{props.user.balance} nCoin</strong>
                    </div>
                </a>
            </div>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        user:state.user.data
    }
}

function mapDispatchToProps(dispatch) {
    return {

    }
}

export default connect(mapStateToProps,mapDispatchToProps)(UserCoin)