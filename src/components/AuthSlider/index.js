import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';

function Image(props) {
    return (
        <img
            className={"demo-chg-bg " + (props.active ? "active" : "")}
            src={props.src}
            alt="Background Image"
        />
    )
}

function AuthSlider(props) {

    useEffect(() => {

    }, []);

    return (
        <div className="demo-bg">
            <div id="demo-bg-list">
                {
                    props.images.map((item, i) => {
                        return (
                            <Image key={i} index={i} active={item.active} src={item.src} />
                        )
                    })
                }
            </div>
        </div>
    )
}

function mapStateToProps(state) {
    return {
    }
}

function mapDispatchToProps(dispatch) {
    return {
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthSlider);