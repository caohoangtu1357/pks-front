import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';

function Footer(props) {

    useEffect(() => {

    }, []);

    return (
        <footer id="footer">
          <div className="show-fixed pull-right">
            You have <a href="#" className="text-bold text-main"><span className="label label-danger">3</span> pending
              action.</a>
          </div>
          <p className="pad-lft">&#0169; CÔNG TY TNHH MTV VIỄN THÔNG QUỐC TẾ FPT</p>
        </footer>
    )
}

function mapStateToProps(state) {
    return {
    }
}

function mapDispatchToProps(dispatch) {
    return {
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Footer);