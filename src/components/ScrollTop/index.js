import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';

function ScrollTop(props) {

    useEffect(() => {

    }, []);

    return (
        <button className="scroll-top btn">
            <i className="pci-chevron chevron-up"></i>
        </button>
    )
}

function mapStateToProps(state) {
    return {
    }
}

function mapDispatchToProps(dispatch) {
    return {
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ScrollTop);