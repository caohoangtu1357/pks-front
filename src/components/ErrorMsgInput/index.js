export default function ErrorInput(props) {
    return (
        <label
            id="problem-error"
            className="error"
            htmlFor="problem"
            style={{ display: "block"}}
        >{props.msg}</label>
    )
}