import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { LOGOUT } from '../../config/ActionTypes/Auth';
import {Link} from "react-router-dom";

function BrandLogo() {
    return (
        <div className="navbar-header">
            <Link to="/" className="navbar-brand">
                <img
                    src="/nifty/img/logo.png"
                    alt="Nifty Logo"
                    className="brand-icon"
                />
                <div className="brand-title">
                    <span className="brand-text">CMS - PKS</span>
                </div>
            </Link>
        </div>
    )
}

function NavigationToggle() {
    return (
        <li className="tgl-menu-btn">
            <a className="mainnav-toggle" href="#">
                <i className="demo-pli-view-list" />
            </a>
        </li>
    )
}

function Notification() {
    return (
        <div className="dropdown-menu dropdown-menu-md dropdown-menu-right">
            <div className="pad-all bord-btm">
                <p className="text-semibold text-main mar-no">Thông báo</p>
            </div>
            <div
                className="nano scrollable has-scrollbar"
                style={{ minHeight: 65 }}
            >
                <div
                    className="nano-content"
                    tabIndex={0}
                    style={{ right: "-15px" }}
                >
                    <ul className="head-list">
                        <li className="bg-gray">
                            <a className="media" href="#">
                                <div className="media-left">
                                    <img
                                        className="img-circle img-sm"
                                        alt="Profile Picture"
                                        src="/img/profile-photos/10.png"
                                    />
                                </div>
                                <div className="media-body">
                                    <div className="text-nowrap">
                                        <strong>Admin</strong> cảm ơn đã tưởng
                                    </div>
                                    <small className="text-muted">Just now</small>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div className="nano-pane" style={{ display: "none" }}>
                    <div
                        className="nano-slider"
                        style={{ height: 20, transform: "translate(0px, 0px)" }}
                    />
                </div>
            </div>
            {/*Dropdown footer*/}
            <div className="pad-all bord-top">
                <a href="#" className="btn-link text-dark box-block">
                    Close
                </a>
            </div>
        </div>
    )
}

function UserDropdown(props) {

    function logoutBtn(e){
        props.logout();
    }

    return (
        <>
            <li className="dropdown">
                <a
                    href="#"
                    data-toggle="dropdown"
                    className="dropdown-toggle"
                    aria-expanded="false"
                >
                    <i className="demo-pli-bell" />
                    <span className="badge badge-header badge-danger" />
                </a>
                <Notification />
            </li>
            <li id="dropdown-user" className="dropdown">
                <a
                    href="#"
                    data-toggle="dropdown"
                    className="dropdown-toggle text-right"
                >
                    <span className="pull-right">
                        <img
                            className="img-circle img-user media-object"
                            src="/img/profile-photos/10.png"
                        />
                    </span>
                    <div className="username hidden-xs">{props.user.full_name}</div>
                </a>
                <div className="dropdown-menu dropdown-menu-md dropdown-menu-right panel-default">
                    <div className="pad-all bord-btm">
                        <p className="text-main mar-btm">Xin chào, {props.user.full_name}</p>
                    </div>
                    <ul className="head-list">
                        <li>
                            <Link to={"/user"}>
                                <i className="demo-pli-male icon-lg icon-fw" /> Thông tin của bạn
                            </Link>
                        </li>
                        <li>
                            <Link to={"/user"}>
                                <i
                                    className="fa fa-key"
                                    style={{ fontSize: 16, paddingRight: 4 }}
                                />Thay đổi mật khẩu
                            </Link>
                        </li>
                    </ul>
                    <div className="pad-all text-right">
                        <button type="button" onClick={logoutBtn} className="btn btn-primary btn-rounded">
                            <i className="demo-pli-unlock" /> Thoát
                        </button>
                    </div>
                </div>
            </li>
        </>
    )
}

function Header(props) {

    return (
        <header id="navbar">
            <div id="navbar-container" className="boxed">
                <BrandLogo />
                <div className="navbar-content clearfix">
                    <ul className="nav navbar-top-links pull-left">
                        <NavigationToggle />
                    </ul>
                    <ul className="nav navbar-top-links pull-right">
                        <UserDropdown user={props.user} logout={props.logout} />
                    </ul>
                </div>
            </div>
        </header>
    )
}

function mapStateToProps(state) {
    return {
        user:state.user.data
    }
}

function mapDispatchToProps(dispatch) {
    return {
        logout:()=>dispatch({type:LOGOUT})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);