import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

function SideBar(props) {
    return (
        <nav id="mainnav-container">
            <div id="mainnav">
                <div id="mainnav-menu-wrap">
                    <div className="nano">
                        <div className="nano-content">
                            
                                <ul id="mainnav-menu" className="list-group">
                                    <li className="active">
                                        <a href="#">
                                            <i className="fa fa-pie-chart" aria-hidden="true" />
                                            <span className="menu-title">
                                                <strong>Hệ thống</strong>
                                            </span>
                                            <i className="arrow" />
                                        </a>
                                        <ul className="collapse" className="active">
                                            <li>
                                                <Link to={"/"}>
                                                    <i className="fa fa-home" aria-hidden="true" />
                                                    Trang chủ
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to={"/recharge"}>
                                                    <i className="fa fa-money" aria-hidden="true" />
                                                    Nạp tiền
                                                </Link>
                                            </li>
                                            <li className="">
                                                <Link to={"/order"}>
                                                    <i className="fa fa-exchange" aria-hidden="true" />
                                                    Lịch sử giao dịch
                                                </Link>
                                            </li>
                                            <li className="">
                                                <Link to={"/user"}>
                                                    <i className="fa fa-user" aria-hidden="true" />
                                                    Cài đặt tài khoản
                                                </Link>
                                            </li>
                                            <li className="">
                                                <Link to={"/contact"}>
                                                    <i className="fa fa-headphones" aria-hidden="true" />
                                                    Liên hệ hổ trợ
                                                </Link>
                                            </li>
                                        </ul>
                                    </li>
                                    <li className="active">
                                        <a href="#">
                                            <i className="fa fa-facebook" aria-hidden="true" />
                                            <span className="menu-title">
                                                <strong>Facebook</strong>
                                            </span>
                                            <i className="arrow" />
                                        </a>
                                        <ul className="collapse" className="active">
                                            <li className="">
                                                <Link to={"/buff-like"}>
                                                    <i className="fa fa-thumbs-o-up" aria-hidden="true" />
                                                    Buff Like bài viết
                                                </Link>
                                            </li>
                                            <li className="">
                                                <Link to={"/buff-comment"}>
                                                    <i className="fa fa-commenting-o" aria-hidden="true" />
                                                        Buff Cmt bài viết
                                                </Link>
                                            </li>
                                            <li className="">
                                                <Link to={"/buff-share"}>
                                                    <i className="fa fa-share-square" aria-hidden="true" />
                                                        Buff Share bài viết
                                                </Link>
                                            </li>
                                            <li className="">
                                                <Link to={"/buff-livestream"}>
                                                    <i className="fa fa-eye" aria-hidden="true" />
                                                        Buff Livestream
                                                </Link>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    )
}

function mapStateToProps(state) {
    return {
    }
}

function mapDispatchToProps(dispatch) {
    return {
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);