import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import "./style.scss";

function Alert(props){
    return (
        <div className={`alert alert-${props.alert.variant} custom-alert`} style={{display:props.alert.display}}>
            <button className="close" data-dismiss="alert"><i className="pci-cross pci-circle"></i></button>
			<strong>Thông báo!</strong><br/> {props.alert.content}
        </div>
    )
}

function mapStateToProps(state) {
    return {
        alert:state.alert.data
    }
}

function mapDispatchToProps(dispatch) {
    return {
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Alert);