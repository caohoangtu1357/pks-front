import {
    GET_INFO_SUCCESS,
    GET_INFO_FAIL
} from "../config/ActionTypes/User";
 
 const init = {
     data:null
 };
 
 export default function authReducer(state= init, action){
     switch (action.type){
        case GET_INFO_SUCCESS:
            return {
                ...state,
                data:action.data
            };
        case GET_INFO_FAIL:
            return {
                ...state,
                data:null
            }
         default:
             return state;
     }
 }