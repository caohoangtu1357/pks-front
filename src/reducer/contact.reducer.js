import {
    CREATE_CONTACT_SUCCESS,
    GET_CONTACT_ALL_SUCCESS,
    GET_CONTACT_DETAIL_SUCCESS,
    UPDATE_CONTACT_SUCCESS,
    STATISTIC_CONTACT_SUCCESS
} from "../config/ActionTypes/Contact";

const init = {
    data: {},
    allContact:[],
    statistic:{
        waiting:0,
        processing:0,
        processed:0   
    }
}

export default function contactReducer(state = init, action) {
    switch (action.type) {
        case CREATE_CONTACT_SUCCESS:
            return {
                ...state,
                data:null
            }
        case GET_CONTACT_ALL_SUCCESS:
            return {
                ...state,
                allContact:action.data
            }
        case GET_CONTACT_DETAIL_SUCCESS:
            return{
                ...state,
                data:action.data
            }
        case UPDATE_CONTACT_SUCCESS:
            return{
                ...state,
                data:null
            }
        case STATISTIC_CONTACT_SUCCESS:
            return{
                ...state,
                statistic:{
                    waiting:action.data.total.WAITING,
                    processing:action.data.total.PROCESSING,
                    processed:action.data.total.PROCESSED  
                }
            }
        default:
            return state;
    }
}