import {
    CREATE_BUFF_LIVE_STREAM_SUCCESS,
    GET_BUFF_LIVE_STREAM_ALL_SUCCESS
} from "../config/ActionTypes/BuffLiveStream";

const init = {
    data: {
        link: "",
        num_view: 0,
        time_view: 30,
        num_comment: 0,
        num_reaction: 0,
        comments: "",
        note: ""
    },
    allBuffLivestream:[]
}

export default function buffLivestreamReducer(state = init, action) {
    switch (action.type) {
        case CREATE_BUFF_LIVE_STREAM_SUCCESS:
            return {
                ...state,
                data:{
                    link: "",
                    num_view: 0,
                    time_view: 30,
                    num_comment: 0,
                    num_reaction: 0,
                    comments: "",
                    note: ""
                }
            }
        case GET_BUFF_LIVE_STREAM_ALL_SUCCESS:
            return {
                ...state,
                allBuffLivestream:action.data
            }
        default:
            return state;
    }
}