import {
    GET_ORDER_ALL_SUCCESS
} from "../config/ActionTypes/Order";

const init = {
    data: null,
    allOrder:[]
}

export default function orderReducer(state = init, action) {
    switch (action.type) {
        case GET_ORDER_ALL_SUCCESS:
            return {
                ...state,
                allOrder:action.data
            }
        default:
            return state;
    }
}