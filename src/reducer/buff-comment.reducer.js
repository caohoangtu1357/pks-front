import {
    CREATE_BUFF_COMMENT_SUCCESS,
    GET_BUFF_COMMENT_ALL_SUCCESS
} from "../config/ActionTypes/BuffComment";

const init = {
    data: {
        post_id:"",
        number:10,
        comments:"",
        note:""
    },
    allBuffComment:[]
}

export default function buffCommentReducer(state = init, action) {
    switch (action.type) {
        case CREATE_BUFF_COMMENT_SUCCESS:
            return {
                ...state,
                data:{
                    post_id:"",
                    number:10,
                    comments:"",
                    note:""
                }
            }
        case GET_BUFF_COMMENT_ALL_SUCCESS:
            return {
                ...state,
                allBuffComment:action.data
            }
        default:
            return state;
    }
}