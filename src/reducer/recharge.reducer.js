import {
    GET_RECHARGE_SUCCESS
} from "../config/ActionTypes/Recharge";

const init = {
    data: {
        total:0,
        total_amount_current_month:0
    }
}

export default function RechargeRuducer(state = init, action) {
    switch (action.type) {
        case GET_RECHARGE_SUCCESS:
            return {
                ...state,
                data:action.data
            }
        default:
            return state;
    }
}