import {
    CREATE_BUFF_LIKE_SUCCESS,
    GET_BUFF_LIKE_ALL_SUCCESS
} from "../config/ActionTypes/BuffLike";

const init = {
    data: {
        post_id:"",
        number:10,
        note:""
    },
    allBuffLike:[],
    callGetAll:false
}

export default function buffLikeReducer(state = init, action) {
    switch (action.type) {
        case CREATE_BUFF_LIKE_SUCCESS:
            return {
                ...state,
                data:{
                    post_id:"",
                    number:10,
                    note:""
                }
            }
        case GET_BUFF_LIKE_ALL_SUCCESS:
            return {
                ...state,
                allBuffLike:action.data
            }
        default:
            return state;
    }
}