import {
    LOGIN_SUCCESS
} from "../config/ActionTypes/Auth";
 
 const init = {
     data:null
 };
 
 export default function authReducer(state= init, action){
     switch (action.type){
        case LOGIN_SUCCESS:
            return {
            ...state,
            data:action.data
            };
         default:
             return state;
     }
 }