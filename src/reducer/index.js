import { combineReducers } from "redux";
import authReducer from "./auth.reducer";
import RechargeRuducer from "./recharge.reducer";
import userReducer from "./user.reducer";
import alertReducer from "./alert.reducer";
import contactReducer from "./contact.reducer";
import rootOrder from "./order.reducer";
import buffLikeReducer from "./buff-like.reducer";
import buffShareReducer from "./buff-share.reducer";
import buffCommentReducer from "./buff-comment.reducer";
import buffLivestreamReducer from "./buff-livestream.reducer";
import postReducer from "./post.reducer";
import notifyReducer from "./notify.reducer";
import pageTitleReducer from "./page-title.reducer";

const rootReducer = combineReducers({
    auth:authReducer,
    user:userReducer,
    recharge:RechargeRuducer,
    alert:alertReducer,
    contact:contactReducer,
    order:rootOrder,
    buffLike:buffLikeReducer,
    buffShare:buffShareReducer,
    buffComment:buffCommentReducer,
    buffLivestream:buffLivestreamReducer,
    post:postReducer,
    notify:notifyReducer,
    title: pageTitleReducer
});

export default rootReducer;