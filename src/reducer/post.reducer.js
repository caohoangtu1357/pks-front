import { CLEAN_POSTS } from "../config/ActionTypes/Notify";
import {
    LOAD_MORE_POST_SUCCESS
} from "../config/ActionTypes/Post";

const init = {
    data: null,
    posts:[]
}

export default function postReducer(state = init, action) {
    switch (action.type) {
        case LOAD_MORE_POST_SUCCESS:
            return {
                ...state,
                posts:[...state.posts,...action.data]
            }
        case CLEAN_POSTS:
            return {
                ...state,
                posts:[]
            }
        default:
            return state;
    }
}