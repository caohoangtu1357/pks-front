import {
    CREATE_BUFF_SHARE_SUCCESS,
    GET_BUFF_SHARE_ALL_SUCCESS
} from "../config/ActionTypes/BuffShare";

const init = {
    data: {
        post_id:"",
        number:10,
        note:""
    },
    allBuffShare:[]
}

export default function buffShareReducer(state = init, action) {
    switch (action.type) {
        case CREATE_BUFF_SHARE_SUCCESS:
            return {
                ...state,
                data: {
                    post_id:"",
                    number:10,
                    note:""
                }
            }
        case GET_BUFF_SHARE_ALL_SUCCESS:
            return {
                ...state,
                allBuffShare:action.data
            }
        default:
            return state;
    }
}