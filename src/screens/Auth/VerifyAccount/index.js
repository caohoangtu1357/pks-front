import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { AuthSlider } from "../../../components";
import { VERIFY_ACCOUNT } from "../../../config/ActionTypes/Auth";
import { useForm } from 'react-hook-form';
import { ErrorInput } from '../../../components';

const images = [
    {
        src: "/nifty/img/bg-img/thumbs/bg-img-1.jpg",
        active: false
    },
    {
        src: "/nifty/img/bg-img/thumbs/bg-img-2.jpg",
        active: true
    },
    {
        src: "/nifty/img/bg-img/thumbs/bg-img-3.jpg",
        active: false
    },
    {
        src: "/nifty/img/bg-img/thumbs/bg-img-4.jpg",
        active: false
    },
    {
        src: "/nifty/img/bg-img/thumbs/bg-img-5.jpg",
        active: false
    },
    {
        src: "/nifty/img/bg-img/thumbs/bg-img-6.jpg",
        active: false
    },
    {
        src: "/nifty/img/bg-img/thumbs/bg-img-7.jpg",
        active: false
    }
];

function VerifyAccount(props) {

    const { register, handleSubmit, formState: { errors }, reset } = useForm();

    function onSubmit(data,e){
        props.verifyAccount({
            phone: data.phone,
            token: data.token
        },props)
    }

    return (
        <div id="container" className="cls-container">
            <div
                id="bg-overlay"
                className="bg-img"
                style={{ backgroundImage: 'url("/nifty/img/bg-img/bg-img-2.jpg")' }}
            />
            <div className="cls-content">
                <div className="cls-content-sm panel">
                    <div className="panel-body">
                        <div className="alert alert-success fade in">
                            <button className="close" data-dismiss="alert">
                                <span>×</span>
                            </button>
                            Hãy nhập token mà chúng tôi đã gửi cho bạn.
                        </div>
                        <div className="mar-ver pad-btm">
                            <h3 className="h4 mar-no">Xác thực tài khoản</h3>
                        </div>
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div className="form-group">
                                <input
                                    {...register("phone", { required: true })}
                                    name="phone"
                                    defaultValue=""
                                    type="text"
                                    placeholder="Số điện thoại"
                                    className="form-control"
                                />
                                {errors.phone?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
                            </div>
                            <div className="form-group">
                                <input
                                    {...register("token", { required: true })}
                                    name="token"
                                    type="text"
                                    placeholder="Mã xác thực"
                                    className="form-control"
                                />
                                {errors.token?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
                            </div>
                            <button className="btn btn-primary btn-lg btn-block" type="submit">
                                Xác thực
                            </button>
                            <div className="pad-all">
                                <Link to={"/login"} className="btn-link">Đăng nhập với tài khoản khác</Link>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <AuthSlider images={images} />
        </div>
    )
}

function mapStateToProps(state) {
    return {
    }
}

function mapDispatchToProps(dispatch) {
    return {
        verifyAccount: (credentials,props) => dispatch({ type: VERIFY_ACCOUNT, credentials ,props})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(VerifyAccount);