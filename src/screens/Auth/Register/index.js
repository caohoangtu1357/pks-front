import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { AuthSlider } from "../../../components";
import { useForm } from "react-hook-form";
import { ErrorInput } from '../../../components';
import "./style.scss";
import { REGISTER } from '../../../config/ActionTypes/Auth';

const images = [
  {
    src: "/nifty/img/bg-img/thumbs/bg-img-1.jpg",
    active: false
  },
  {
    src: "/nifty/img/bg-img/thumbs/bg-img-2.jpg",
    active: true
  },
  {
    src: "/nifty/img/bg-img/thumbs/bg-img-3.jpg",
    active: false
  },
  {
    src: "/nifty/img/bg-img/thumbs/bg-img-4.jpg",
    active: false
  },
  {
    src: "/nifty/img/bg-img/thumbs/bg-img-5.jpg",
    active: false
  },
  {
    src: "/nifty/img/bg-img/thumbs/bg-img-6.jpg",
    active: false
  },
  {
    src: "/nifty/img/bg-img/thumbs/bg-img-7.jpg",
    active: false
  }
];

function Register(props) {

  const { register, handleSubmit, formState: { errors }, reset } = useForm();

  function onSubmit(data, e) {
    props.register({
      password: data.password,
      full_name: data.full_name,
      email: data.email,
      phone: data.phone,
      facebook_link: data.facebook_link,
      password_confirm: data.password_confirm,
      is_accepted: data.is_accepted
    }, props);
  }

  return (
    <div id="container" className="cls-container">
      <div
        id="bg-overlay"
        className="bg-img"
        style={{ backgroundImage: 'url("/nifty/img/bg-img/bg-img-2.jpg")' }}
      />
      <div className="cls-content">
        <div className="cls-content-lg panel">
          <div className="panel-body">
            <div className="mar-ver pad-btm">
              <h3 className="h4 mar-no">Tạo tài khoản </h3>
            </div>
            <form onSubmit={handleSubmit(onSubmit)} className="form-register">
              <input type="hidden" defaultValue name="_token" />
              <div className="row">
                <div className="col-sm-6">
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Họ và tên"
                      name="full_name"
                      defaultValue=""
                      {...register("full_name", { required: true })}
                    />
                    {errors.full_name?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
                  </div>
                  <div className="form-group">
                    <input
                      type="email"
                      className="form-control"
                      placeholder="E-mail"
                      name="email"
                      defaultValue=""
                      {...register("email", { required: true })}
                    />
                    {errors.email?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
                  </div>
                  <div className="form-group">
                    <input
                      type="tel"
                      className="form-control"
                      placeholder="Số điện thoại"
                      name="phone"
                      defaultValue=""
                      pattern="[0-9]{10}|[0-9]{11}"
                      title="Số điện thoại từ 10 đến 11 kí tự."
                      {...register("phone", { required: true })}
                    />
                    {errors.phone?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
                  </div>
                </div>
                <div className="col-sm-6">
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Link facebook"
                      name="facebook_link"
                      defaultValue=""
                      {...register("facebook_link", { required: true })}
                    />
                    {errors.facebook_link?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
                  </div>
                </div>
                <div className="col-sm-6">
                  <div className="form-group">
                    <input
                      type="password"
                      className="form-control"
                      placeholder="Mật khẩu"
                      name="password"
                      {...register("password", { required: true, minLength: 8 })}
                    />
                    {errors.password?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
                    {errors.password?.type === "minLength" && <ErrorInput msg="Mật khẩu tối thiểu 8 kí tự." />}
                  </div>
                </div>
                <div className="col-sm-6">
                  <div className="form-group">
                    <input
                      type="password"
                      className="form-control"
                      placeholder="Nhập lại mật khẩu"
                      name="password_confirm"
                      {...register("password_confirm", { required: true, min: 8 })}
                    />
                    {errors.password_confirm?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
                    {errors.password_confirm?.min === "required" && <ErrorInput msg="Mật khẩu tối thiểu 8 kí tự." />}
                  </div>
                </div>
              </div>
              <div className="checkbox pad-btm text-left">
                <input
                  name="is_accepted"
                  id="demo-form-checkbox"
                  className="magic-checkbox"
                  type="checkbox"
                  {...register("is_accepted", { required: true })}
                />
                <label htmlFor="demo-form-checkbox">
                  Tôi đồng ý với
                  <Link to="#" className="btn-link"><strong className="text-primary"> Điều khoản và dịch vụ.</strong></Link>
                </label>
                {errors.is_accepted?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
              </div>
              <button className="btn btn-primary btn-block" type="submit">
                Đăng kí
              </button>
            </form>
          </div>
          <div className="pad-all">
            Bạn đã có tài khoản ?{" "}
            <Link className="btn-link mar-rgt" to={"login"}>Đăng nhập</Link>
          </div>
        </div>
        <AuthSlider images={images} />
      </div>
    </div>
  )
}

function mapStateToProps(state) {
  return {
  }
}

function mapDispatchToProps(dispatch) {
  return {
    register: (credentials, props) => dispatch({ type: REGISTER, credentials: credentials, props: props })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Register);