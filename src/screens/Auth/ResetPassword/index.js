import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom/cjs/react-router-dom.min';
import { AuthSlider, ErrorInput } from '../../../components';
import { useForm } from 'react-hook-form';
import "./style.scss";
import { RESET_PASSWORD } from '../../../config/ActionTypes/Auth';

const images = [
  {
    src: "/nifty/img/bg-img/thumbs/bg-img-1.jpg",
    active: false
  },
  {
    src: "/nifty/img/bg-img/thumbs/bg-img-2.jpg",
    active: true
  },
  {
    src: "/nifty/img/bg-img/thumbs/bg-img-3.jpg",
    active: false
  },
  {
    src: "/nifty/img/bg-img/thumbs/bg-img-4.jpg",
    active: false
  },
  {
    src: "/nifty/img/bg-img/thumbs/bg-img-5.jpg",
    active: false
  },
  {
    src: "/nifty/img/bg-img/thumbs/bg-img-6.jpg",
    active: false
  },
  {
    src: "/nifty/img/bg-img/thumbs/bg-img-7.jpg",
    active: false
  }
];

function ResetPassword(props) {

  const { register, handleSubmit, formState: { errors }, reset } = useForm();

  function onSubmit(data, e) {
    props.resetPassword({
      phone: data.phone,
      new_password: data.new_password,
      confirm_password: data.confirm_password
    }, props)
  }

  return (
    <div id="container" className="cls-container">
      <div
        id="bg-overlay"
        className="bg-img"
        style={{ backgroundImage: 'url("/nifty/img/bg-img/bg-img-2.jpg")' }}
      />
      <div className="cls-content">
        <div className="cls-content-sm panel">
          <div className="panel-body">
            <div className="mar-ver pad-btm">
              <h3 className="h4 mar-no">Đặt lại mật khẩu</h3>
            </div>
            <form onSubmit={handleSubmit(onSubmit)} className="form-reset-password">
              <div className="form-group">
                <input
                  {...register("phone", { required: true })}
                  name="phone"
                  type="text"
                  placeholder="Số điện thoại"
                  className="form-control"
                />
                {errors.phone?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
              </div>
              <div className="form-group">
                <input
                  {...register("new_password", { required: true })}
                  name="new_password"
                  type="password"
                  placeholder="Mật khẩu mới"
                  className="form-control"
                />
                {errors.new_password?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
              </div>
              <div className="form-group">
                <input
                  {...register("confirm_password", { required: true })}
                  name="confirm_password"
                  type="password"
                  placeholder="Nhập lại mật khẩu"
                  className="form-control"
                />
                {errors.confirm_password?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
              </div>
              <button className="btn btn-primary btn-lg btn-block" type="submit">
                Đặt lại mật khẩu
              </button>
              <div className="pad-all">
                <Link to={"/login"} className="btn-link mar-rgt">Đăng nhập?</Link>
                <Link to={"/register"} className="btn-link mar-lft">Đăng kí tài khoản</Link>
              </div>
            </form>
          </div>
        </div>
        <AuthSlider images={images} />
      </div>
    </div>
  )
}

function mapStateToProps(state) {
  return {
  }
}

function mapDispatchToProps(dispatch) {
  return {
    resetPassword: (credentials,props)=>dispatch({type:RESET_PASSWORD,credentials,props})
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);