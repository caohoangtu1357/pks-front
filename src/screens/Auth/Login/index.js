import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { AuthSlider } from "../../../components";
import { LOGIN } from "../../../config/ActionTypes/Auth";
import { useForm } from 'react-hook-form';
import { ErrorInput } from '../../../components';
import "./style.scss";

const images = [
  {
    src: "/nifty/img/bg-img/thumbs/bg-img-1.jpg",
    active: false
  },
  {
    src: "/nifty/img/bg-img/thumbs/bg-img-2.jpg",
    active: true
  },
  {
    src: "/nifty/img/bg-img/thumbs/bg-img-3.jpg",
    active: false
  },
  {
    src: "/nifty/img/bg-img/thumbs/bg-img-4.jpg",
    active: false
  },
  {
    src: "/nifty/img/bg-img/thumbs/bg-img-5.jpg",
    active: false
  },
  {
    src: "/nifty/img/bg-img/thumbs/bg-img-6.jpg",
    active: false
  },
  {
    src: "/nifty/img/bg-img/thumbs/bg-img-7.jpg",
    active: false
  }
];

function Login(props) {

  const { register, handleSubmit, formState: { errors }} = useForm();

  function onSubmit(data,e) {
    props.loginAction({
      phone: data.phone,
      password: data.password,
      is_remember_me: data.is_remember_me
    })
  }

  return (
    <div id="container" className="cls-container">
      <div
        id="bg-overlay"
        className="bg-img"
        style={{ backgroundImage: 'url("/nifty/img/bg-img/bg-img-2.jpg")' }}
      />
      <div className="cls-content">
        <div className="cls-content-sm panel">
          <div className="panel-body">
            <div className="mar-ver pad-btm">
              <h3 className="h4 mar-no">Đăng nhập hệ thống</h3>
            </div>
            <form onSubmit={handleSubmit(onSubmit)} className="form-login">
              <div className="form-group">
                <input
                  {...register("phone", { required: true })}
                  name="phone"
                  defaultValue=""
                  type="text"
                  placeholder="Số điện thoại"
                  className="form-control"
                  autoFocus
                />
                {errors.phone?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
              </div>
              <div className="form-group">
                <input
                  {...register("password", { required: true })}
                  name="password"
                  type="password"
                  placeholder="Password"
                  className="form-control"
                />
              </div>
              {errors.password?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
              <div className="checkbox pad-btm text-left">
                <input
                  {...register("is_remember_me")}
                  defaultValue={0}
                  id="demo-form-checkbox"
                  className="magic-checkbox"
                  type="checkbox"
                  name="is_remember_me"
                />
                <label htmlFor="demo-form-checkbox">Ghi nhớ</label>
              </div>
              <button className="btn btn-primary btn-lg btn-block" type="submit" autoFocus>
                Đăng nhập
              </button>
              <div className="pad-all">
                <Link to={"/reset-password"} className="btn-link mar-rgt">Quên mật khẩu ?</Link>
                <Link to={"/register"} className="btn-link mar-lft">Đăng kí tài khoản</Link>
              </div>
            </form>
          </div>
        </div>
      </div>
      <AuthSlider images={images} />
    </div>
  )
}

function mapStateToProps(state) {
  return {
  }
}

function mapDispatchToProps(dispatch) {
  return {
    loginAction: (credentials) => dispatch({ type: LOGIN, credentials })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);