import React, { useState, useEffect } from 'react';
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from 'react-bootstrap-table2-paginator';
import { connect } from "react-redux";
import { BuffUserCoin ,ErrorInput} from "../../components";
import { ALERT } from '../../config/ActionTypes/Alert';
import { CREATE_BUFF_COMMENT, GET_BUFF_COMMENT_ALL } from "../../config/ActionTypes/BuffComment";
import { processIdPost } from "../../config/General";
import { useForm } from "react-hook-form";
import { setPageTitle } from '../../actions/page-title.action';

function PanelNote() {
    return (
        <div className="panel">
            <div className="list-group bg-trans">
                <div className="list-group-item">
                    <div className="alert alert-primary" style={{ fontSize: 16 }}>
                        <strong>Thông tin</strong>
                        <br />
                        <small>
                            - Tất cả loại Comment Buff đều sử dụng tài khoản Facebook Việt có
                            Avatar để tăng Cmt.
                            <br />
                            - Đối với loại Comment Buff Trực Tiếp sẽ có tỉ lệ tụt nếu nick của
                            họ bị Checkpoint.
                            <br />- Các đơn Buff sẽ không được hoàn tiền với bất kì lý do gì,
                            cân nhắc trước khi Order.
                        </small>
                    </div>
                    <div className="alert alert-danger" style={{ fontSize: 16 }}>
                        <strong>Lưu ý</strong>
                        <br />
                        <small>
                            - Nghiêm cấm bình luận những nội có cử chỉ, lời nói thô bạo, khiêu
                            khích, trêu ghẹo, xúc phạm nhân phẩm, danh dự của Cá nhân hoặc Tổ
                            chức.
                            <br />- Ngiêm cấm Buff các ID Seeding có nội dung vi phạm pháp
                            luật, chính trị, đồi trụy... Nếu cố tình buff bạn sẽ bị trừ hết
                            tiền và banned khỏi hệ thống vĩnh viễn, và phải chịu hoàn toàn
                            trách nhiệm trước pháp luật.
                        </small>
                    </div>
                </div>
            </div>
        </div>
    )
}

function PanelTable(props) {

    const [page, setPage] = useState(1);
    const [sizePerPage, setSizePerPage] = useState(10);
    const [sort, setSort] = useState('created_at');
    const [order, setOrder] = useState('desc');
    const allBuffComment = props.allBuffComment;

    useEffect(() => {
        props.getAll({
            limit: sizePerPage,
            offset: 0,
            sort: sort,
            order: order,
        });
    }, [props.refeshTable])

    function onTableChange(type, newState) {
        props.getAll({
            sort: newState.sortField,
            order: newState.sortOrder,
            limit: newState.sizePerPage,
            offset: (newState.page - 1) * newState.sizePerPage,
        });
        setSort(newState.sortField);
        setOrder(newState.sortOrder);
        setPage(newState.page);
        setSizePerPage(newState.sizePerPage);
    }

    const paginateOptions = {
        page: page,
        sizePerPage: sizePerPage,
        showTotal: true,
        totalSize: allBuffComment.length != 0 ? allBuffComment.total : 0,
        hidePageListOnlyOnePage:true,
        paginationTotalRenderer:(from, to, size)=>{
            return(
                <span className="react-bootstrap-table-pagination-total"> Hiển thị từ { from } đến { to } của { size }</span>
            )
        }
    };

    const columns = [{
        dataField: 'post_id',
        text: 'ID Bài viết',
        sort: true
    }, {
        dataField: 'number',
        text: 'Số lượng',
        sort: true
    }, {
        dataField: 'status',
        text: 'Trạng thái',
        sort: true,
        headerAlign: 'center',
        align: "center"
    }];

    if (allBuffComment.length == 0) {
        return (
            <div style={{ textAlign: "center" }}>
                <p>Bạn chưa có giao dịch nào được thực hiện</p>
            </div>
        )
    } else {
        if (allBuffComment.total == 0) {
            return (
                <div style={{ textAlign: "center", fontSize: 16 }}>
                    <p>Bạn chưa có giao dịch nào được thực hiện</p>
                </div>
            )
        }
        return (
            <div className="panel-body">
                <div className="overflow-hidden" style={{ clear: "both" }}>
                    <BootstrapTable
                        onTableChange={onTableChange}
                        keyField='_id'
                        remote={true}
                        data={allBuffComment.length != 0 ? allBuffComment.rows : []}
                        columns={columns}
                        pagination={paginationFactory(paginateOptions)}
                        keyBoardNav
                    />
                </div>
            </div>
        )
    }
}

function PanelFormBuff(props) {

    const [idPost, setIdPost] = useState("");
    const [number, setNumber] = useState(10);
    const [comments, setComments] = useState("");
    const [note, setNote] = useState("");
    const[total,setTotal] = useState(400);

    useEffect(() => {
        setIdPost("");
        setNumber(10);
        setNote("");
        setComments("");
        setTotal(1000)
        reset({
            id_post:"",
            number:10,
            note:"",
            comments:""
        });
    }, [props.buffComment])

    const buffComment = props.buffComment;

    const { register, handleSubmit, formState: { errors }, reset } = useForm({
        defaultValues: {
            id_post: buffComment.post_id,
            number: buffComment.number,
            comments: buffComment.comments,
            note: buffComment.note
        }
    });

    function onSubmit(data) {
        var idPostProcessed = processIdPost(props, data.post_id);
        if (!idPostProcessed) {
            return;
        }
        props.create({
            post_id: idPostProcessed,
            number: data.number,
            note: data.note,
            comments: data.comments
        });
        props.setRefreshTable(props.refeshTable ? false : true);
    }

    function handleIdPost(value) {
        var idPostProcessed = processIdPost(props, value);
        if (idPostProcessed) {
            setIdPost(idPostProcessed);
        } else {
            setIdPost(value);
        }
    }

    return (
        <form
            className="form-horizontal"
            onSubmit={handleSubmit(onSubmit)}
        >
            <div className="form-group">
                <label className="col-lg-3 control-label">Link bài viết</label>
                <div className="col-lg-7">
                    <input
                        {...register("post_id", { required: true })}
                        type="text"
                        className="form-control"
                        name="post_id"
                        value={idPost}
                        onChange={e=>handleIdPost(e.target.value)}
                        placeholder="Nhập link bài viết công khai"
                    />
                    {errors.post_id?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
                </div>
            </div>
            <div className="form-group">
                <label className="col-lg-3 control-label">Nội dung</label>
                <div className="col-lg-7">
                    <textarea
                        {...register("comments", { required: true })}
                        className="form-control"
                        name="comments"
                        placeholder="Nhập nội dung bạn muốn tăng bình luận, mỗi nội dung 1 dòng."
                        rows={10}
                        value={comments}
                        onChange={e=>setComments(e.target.value)}
                    />
                    {errors.comments?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
                </div>
            </div>
            <div className="form-group">
                <label className="col-lg-3 control-label">Số lượng</label>
                <div className="col-lg-7">
                    <input
                        {...register("number", { required: true, min: 1 })}
                        type="number"
                        className="form-control"
                        name="number"
                        value={number}
                        onChange={e=>{setNumber(e.target.value); setTotal(e.target.value * 100)}}
                    />
                    {errors.number?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
                    {errors.number?.type === "min" && <ErrorInput msg="Số lượng tối thiểu là 1." />}
                </div>
            </div>
            <div className="form-group">
                <label className="col-lg-3 control-label">Ghi chú </label>
                <div className="col-lg-7">
                    <input
                        {...register("note")}
                        type="text"
                        className="form-control"
                        name="note"
                        placeholder="Ghi chú về đơn hàng của bạn."
                        value={note}
                        onChange={e=>setNote(e.target.value)}
                    />
                </div>
            </div>
            <div className="form-group">
                <div className="col-lg-7 col-lg-offset-3">
                    <div
                        className="alert alert-primary text-center"
                        style={{ fontSize: 16 }}
                    >
                        <span>
                            Tổng: <strong id="totalCoin">{total} nCoin</strong>
                        </span>
                        <br />
                        <span>
                            Giá buff là <strong>100 nCoin</strong>/Comment
                        </span>
                    </div>
                </div>
            </div>
            <div className="panel-footer clearfix">
                <div className="col-lg-7 col-lg-offset-3 text-center">
                    <button
                        type="submit"
                        className="btn btn-mint"
                        name="signup"
                        value="Sign up"
                    >
                        Thanh toán
                    </button>
                </div>
            </div>
        </form>
    )
}

function BuffComment(props) {

    useEffect(()=>{
        props.setPageTitle("Buff Comment","Buff Comment");
    },[])


    const [refeshTable,setRefreshTable] = useState(false);

    return (
        <div id="page-content">
            <div className="col-md-8">
                <div className="panel">
                    <div className="panel-heading" style={{ borderBottom: "1px solid #eee" }}>
                        <div className="panel-control" style={{ float: "left" }}>
                            <ul className="nav nav-tabs">
                                <li className="active">
                                    <a data-toggle="tab" href="#demo-tabs-box-1">
                                        Thêm ID Buff
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#demo-tabs-box-2">
                                        Danh sách Buff
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="panel-body">
                        <div className="tab-content">
                            <div id="demo-tabs-box-1" className="tab-pane fade in active">
                                <PanelFormBuff {...props} setRefreshTable={setRefreshTable} refeshTable={refeshTable}/>
                            </div>
                            <div id="demo-tabs-box-2" className="tab-pane fade">
                                <PanelTable {...props} refeshTable={refeshTable} setRefreshTable={setRefreshTable} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-md-4">
                <BuffUserCoin />
            </div>
            <div className="col-md-4">
                <PanelNote {...props} />
            </div>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        allBuffComment: state.buffComment.allBuffComment,
        buffComment: state.buffComment.data
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getAll: (filters) => dispatch({ type: GET_BUFF_COMMENT_ALL, filters }),
        create: (attributes) => dispatch({ type: CREATE_BUFF_COMMENT, attributes }),
        showAlert: (content, variant) => dispatch({ type: ALERT, info: { content: content, variant: variant } }),
        setPageTitle: (main,breadcrumb_1,breadcrumb_2) =>dispatch(setPageTitle(main,breadcrumb_1,breadcrumb_2))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BuffComment)