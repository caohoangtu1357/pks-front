import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import {setPageTitle} from "../../actions/page-title.action";


function BalanceInfo(props) {
    return (
        <>
            <div className="card">
                <div
                    className="bg-holder bg-card"
                    style={{ backgroundImage: 'url("/images/corner-2.png")' }}
                ></div>
                <blockquote style={{ marginTop: 20 }}>
                    <strong className="text-primary">{props.user.balance} nCoin</strong> <br /> Số dư
                </blockquote>
            </div>
            <div className="card" style={{ marginTop: 20 }}>
                <div
                    className="bg-holder bg-card"
                    style={{ backgroundImage: 'url("/images/corner-1.png")' }}
                ></div>
                <blockquote style={{ marginTop: 20 }}>
                    <strong style={{ color: "#e63757 !important" }}>
                        {props.recharge.total} nCoin
                    </strong>{" "}
                    <br /> Tổng nạp
                </blockquote>
            </div>
            <div className="card" style={{ marginTop: 20 }}>
                <div
                    className="bg-holder bg-card"
                    style={{ backgroundImage: 'url("/images/corner-3.png")' }}
                ></div>
                <blockquote style={{ marginTop: 20 }}>
                    <strong className="text-success">{props.recharge.total_amount_current_month} nCoin</strong> <br /> Tổng nạp
                    trong tháng
                </blockquote>
            </div>
        </>
    )
}


function TabCharge(props) {
    return (
        <div id="demo-tabs-box-1" className="tab-pane fade in active">
            <h3 className="text-primary">Tỷ giá: 1 VND = 1 nCoin</h3>
            <div className="alert alert-dark">
                <span>
                    Bạn vui lòng chuyển khoản chính xác nội dung chuyển khoản bên
                    dưới hệ thống sẽ tự động cộng tiền cho bạn sau 1 - 5 phút sau
                    khi nhận được tiền. Nếu chuyển khác ngân hàng sẽ mất thời gian
                    lâu hơn, tùy thời gian xử lý của mỗi ngân hàng. Nếu sau 10
                    phút từ khi tiền trong tài khoản của bạn bị trừ mà vẫn chưa
                    được cộng tiền vui liên hệ Admin để được hỗ trợ.
                </span>
            </div>
            <div className="row form-bank">
                <div className="col-md-6">
                    <div className="img-bank">
                        <img className="logo-bank" alt="logo-tpbank" src="/images/tpbank.webp" />
                    </div>
                    <div className="alert-bank">
                        <span className="text-bank">
                            Số tài khoản:{" "}
                            <strong className="text-primary">0123456789</strong>
                        </span>
                        <br />
                        <span>
                            Tên tài khoản: <strong>PHAM MINH TIEN</strong>
                        </span>
                        <br />
                        <span>
                            Ngân hàng: <strong>Tiên Phong Bank</strong>
                        </span>
                    </div>
                </div>
                <div className="col-md-6">
                    <div className="img-bank">
                        <img className="logo-bank" src="/images/momo.png" />
                    </div>
                    <div className="alert-bank">
                        <span className="text-bank">
                            Số điện thoại:
                            <strong className="text-primary">0867870179</strong>
                        </span>
                        <br />
                        <span>
                            Tên tài khoản: <strong>PHAM MINH TIEN</strong>
                        </span>
                        <br />
                        <span>
                            Số tiền tối thiểu: <strong>20,000vnđ</strong>
                        </span>
                    </div>
                </div>
            </div>
            <h3 className="text-primary">Nội dung chuyển khoản</h3>
            <div className="alert alert-primary text-center bold">
                <span>NAP {props.user.phone}</span>
            </div>
            <div className="alert alert-warning">
                <strong>Lưu ý:</strong>
                Nạp sai cú pháp hoặc sai số tài khoản sẽ bị trừ 10% phí giao
                dịch, tối đa trừ 50.000 nCoin. Ví dụ nạp sai 100.000 trừ 10.000,
                200.000 trừ 20.000 , 500.000 trừ 50.000, 1 triệu trừ 50.000, 10
                triệu trừ 50.000...
            </div>
        </div>
    )
}

function TabChargeHistory(props) {
    return (
        <div id="demo-tabs-box-2" className="tab-pane fade">
            <p className="text-main text-lg mar-no">Lịch sử nạp</p>
            <div className="row" style={{ padding: 14 }}>
                <div className="overflow-hidden" style={{ clear: "both" }}>
                    <table
                        id="devices-table"
                        className="table table-bordered table-striped table-hover"
                        cellSpacing={0}
                        data-toggle="table"
                        data-locale="vi-VN"
                        data-toolbar="#table-toolbar"
                        data-striped="true"
                        data-url="recharge.data-ajax"
                        data-sort-name="created_at"
                        data-sort-order="desc"
                        data-show-toggle="false"
                        data-show-columns="true"
                        data-pagination="true"
                        data-side-pagination="server"
                        data-page-size
                        data-page-list
                        data-query-params="queryParams"
                        data-cookie="true"
                    >
                        <thead>
                            <tr>
                                <th data-formatter="formatSTT">STT</th>
                                <th data-field="content">Nội dung</th>
                                <th data-field="phone">SĐT</th>
                                <th data-field="amount" data-formatter="formatNumber">
                                    Tổng số
                                </th>
                                <th
                                    data-field="is_activated"
                                    data-align="center"
                                    data-formatter="formatStatus"
                                >
                                    Trạng thái
                                </th>
                                <th
                                    data-field="created_at"
                                    data-formatter="formatDate"
                                    data-align="center"
                                >
                                    Ngày nạp
                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    )
}

function ReCharge(props) {
    useEffect(()=>{
        props.setPageTitle("Nạp tiền","Nạp tiền");
    },[])

    return (
        <div id="page-content" style={{ fontSize: 16 }}>
            <div className="row">
                <div className="col-md-8">
                    <div className="panel panel-primary">
                        <div className="panel-heading">
                            <div className="panel-control">
                                <ul className="nav nav-tabs">
                                    <li className="active">
                                        <a data-toggle="tab" href="#demo-tabs-box-1">
                                            Ngân Hàng
                                        </a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#demo-tabs-box-2">
                                            Lịch sử nạp
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="panel-body">
                            <div className="tab-content">
                                <TabCharge user={props.user} />
                                <TabChargeHistory recharge={props.recharge} />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-4">
                    <BalanceInfo recharge={props.recharge} user={props.user}/>
                </div>
            </div>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        user: state.user.data,
        recharge: state.recharge.data
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setPageTitle: (main,breadcrumb_1,breadcrumb_2) =>dispatch(setPageTitle(main,breadcrumb_1,breadcrumb_2))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReCharge);