import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { CHANGE_PASSWORD, UPDATE_FB_ID } from '../../config/ActionTypes/User';
import {setPageTitle} from "../../actions/page-title.action";

function PanelChangePassword(props) {
    const [oldPassword, setOldPassword] = useState("");
    const [newPassword, setNewPassword] = useState("");
    const [passwordConfirm, setPasswordConfirm] = useState("");

    function clearState() {
        setOldPassword("");
        setNewPassword("");
        setPasswordConfirm("");
    }

    function btnChangePassword() {
        props.changePassword(
            {
                old_password: oldPassword,
                new_password: newPassword,
                password_confirm: passwordConfirm
            }
        );
        clearState();
    }

    return (
        <div className="panel panel-bordered-primary">
            <div className="panel-body">
                <form>
                    <div className="panel-heading">
                        <h4 className="panel-title" style={{ padding: 0 }}>
                            Đổi Mật Khẩu
                        </h4>
                    </div>
                    <div className="form-group">
                        <label htmlFor="password" className="control-label">
                            Mật Khẩu Cũ
                        </label>
                        <input
                            onChange={e => setOldPassword(e.target.value)}
                            type="password"
                            className="form-control"
                            placeholder="Mật khẩu hiện tại"
                            value={oldPassword}
                        />
                        <label
                            id="old_password-error"
                            className="error"
                            htmlFor="old_password"
                            style={{ display: "none" }}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="new_password" className="control-label">
                            Mật Khẩu Mới
                        </label>
                        <input
                            onChange={e => setNewPassword(e.target.value)}
                            type="password"
                            className="form-control"
                            placeholder="Mật khẩu mới"
                            value={newPassword}
                        />
                        <label
                            id="new_password-error"
                            className="error"
                            htmlFor="new_password"
                            style={{ display: "none" }}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="password_confirm" className="control-label">
                            Xác Nhận Mật Khẩu
                        </label>
                        <input
                            onChange={e => setPasswordConfirm(e.target.value)}
                            type="password"
                            className="form-control"
                            placeholder="Xác nhận mật khẩu"
                            value={passwordConfirm}
                        />
                        <label
                            id="password_confirm-error"
                            className="error"
                            htmlFor="password_confirm"
                            style={{ display: "none" }}
                        />
                    </div>
                    <div className="form-group">
                        <button type="button" className="btn btn-primary" onClick={btnChangePassword}>
                            <i className="fa fa-user" /> Đổi mật khẩu
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}

function User(props) {

    useEffect(()=>{
        props.setPageTitle("Cài đặt tài khoản","Cài đặt tài khoản");
    },[])

    const [fbId,setFbId] = useState(props.user.fb_id);

    function updateFbId(){
        props.updateFbId({fb_id:fbId});
    }

    return (
        <div id="page-content">
            <div className="col-md-8">
                <div className="panel panel-bordered-primary">
                    <div className="panel-body">
                        <div className="panel">
                            <div className="panel-heading">
                                <h3 className="panel-title">Thông Tin Tài Khoản</h3>
                            </div>
                            <div className="panel-body">
                                <div className="row">
                                    <div className="form-group col-md-6">
                                        <label htmlFor="title" className="control-label">
                                            Họ Và Tên
                                        </label>
                                        <input
                                            type="text"
                                            id="full_name"
                                            className="form-control"
                                            name="full_name"
                                            defaultValue={props.user.full_name}
                                            readOnly
                                        />
                                        <label
                                            id="full_name-error"
                                            className="error"
                                            htmlFor="full_name"
                                            style={{ display: "none" }}
                                        />
                                    </div>
                                    <div className="form-group col-md-6">
                                        <label htmlFor="contact_id" className="control-label">
                                            Ngày Tham Gia
                                        </label>
                                        <input
                                            type="text"
                                            id="created_at"
                                            name="created_at"
                                            className="form-control"
                                            defaultValue={props.user.created_at}
                                            readOnly
                                        />
                                        <label
                                            id="created_at-error"
                                            className="error"
                                            htmlFor="created_at"
                                            style={{ display: "none" }}
                                        />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="form-group col-md-6">
                                        <label htmlFor="contact_id" className="control-label">
                                            Số Điện Thoại
                                        </label>
                                        <input
                                            type="text"
                                            id="phone"
                                            name="phone"
                                            defaultValue={props.user.phone}
                                            className="form-control"
                                            readOnly
                                        />
                                        <label
                                            id="phone-error"
                                            className="error"
                                            htmlFor="phone"
                                            style={{ display: "none" }}
                                        />
                                    </div>
                                    <div className="form-group col-md-6">
                                        <label htmlFor="contact_id" className="control-label">
                                            ID Facebook
                                        </label>
                                        <label
                                            id="contact_id-error"
                                            className="error"
                                            htmlFor="contact_id"
                                            style={{ display: "none" }}
                                        />
                                        <div className="input-group mar-btm">
                                            <input
                                                type="text"
                                                className="form-control"
                                                placeholder="facebook ID"
                                                value={fbId}
                                                onChange={e=>setFbId(e.target.value)}
                                                defaultValue={props.user.fb_id}
                                            />
                                            <div className="input-group-btn">
                                                <button
                                                    onClick={updateFbId}
                                                    className="btn btn-primary"
                                                    id="btn-update-fb-id"
                                                    type="button"
                                                    aria-expanded="false"
                                                >
                                                    <i className="fa fa-edit" /> Sửa ID
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-md-4">
                <PanelChangePassword {...props} />
            </div>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        user: state.user.data,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        changePassword: (passwordInfo) => dispatch({ type: CHANGE_PASSWORD, passwordInfo }),
        updateFbId: (fbId) => dispatch({type:UPDATE_FB_ID,fbId}),
        setPageTitle: (main,breadcrumb_1,breadcrumb_2) =>dispatch(setPageTitle(main,breadcrumb_1,breadcrumb_2))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(User);