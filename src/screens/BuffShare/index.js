import React, { useState, useEffect } from 'react';
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from 'react-bootstrap-table2-paginator';
import { connect } from "react-redux";
import { BuffUserCoin, ErrorInput } from "../../components";
import { ALERT } from '../../config/ActionTypes/Alert';
import { CREATE_BUFF_SHARE, GET_BUFF_SHARE_ALL } from "../../config/ActionTypes/BuffShare";
import { useForm } from "react-hook-form";
import { processIdPost } from "../../config/General";
import { setPageTitle } from '../../actions/page-title.action';

function PanelNote() {
    return (
        <div className="panel">
            <div className="list-group bg-trans">
                <div className="list-group-item">
                    <div className="alert alert-danger" style={{ fontSize: 16 }}>
                        <strong>Lưu ý</strong>
                        <br />
                        <small>
                            - Share Group chỉ hoạt động từ 9h sáng đến 23h đêm.
                            <br />
                            - Share Profile hoạt động 24/24 và lên ngay sau khi Order vài
                            phút.
                            <br />
                            - Đối với loại Share Buff Trực Tiếp sẽ có tỉ lệ tụt nếu nick của
                            họ bị Checkpoint.
                            <br />- Các đơn Buff sẽ không được hoàn tiền với bất kì lý do gì,
                            cân nhắc trước khi Order.
                        </small>
                    </div>
                </div>
            </div>
        </div>
    )
}

function PanelTable(props) {

    const [page, setPage] = useState(1);
    const [sizePerPage, setSizePerPage] = useState(10);
    const [sort, setSort] = useState('created_at');
    const [order, setOrder] = useState('desc');
    const allBuffShare = props.allBuffShare;

    useEffect(() => {
        props.getAll({
            limit: sizePerPage,
            offset: 0,
            sort: sort,
            order: order,
        });
    }, [props.buffShare])

    function onTableChange(type, newState) {
        props.getAll({
            sort: newState.sortField,
            order: newState.sortOrder,
            limit: newState.sizePerPage,
            offset: (newState.page - 1) * newState.sizePerPage,
        });
        setSort(newState.sortField);
        setOrder(newState.sortOrder);
        setPage(newState.page);
        setSizePerPage(newState.sizePerPage);
    }

    const paginateOptions = {
        page: page,
        sizePerPage: sizePerPage,
        showTotal: true,
        totalSize: allBuffShare.length != 0 ? allBuffShare.total : 0,
        hidePageListOnlyOnePage:true,
        paginationTotalRenderer:(from, to, size)=>{
            return(
                <span className="react-bootstrap-table-pagination-total"> Hiển thị từ { from } đến { to } của { size }</span>
            )
        }
    };

    const columns = [{
        dataField: 'post_id',
        text: 'ID Bài viết',
        sort: true
    }, {
        dataField: 'number',
        text: 'Số lượng',
        sort: true
    }, {
        dataField: 'status',
        text: 'Trạng thái',
        sort: true,
        headerAlign: 'center',
        align: "center"
    }];

    if (allBuffShare.length == 0) {
        return (
            <div style={{ textAlign: "center" }}>
                <p>Bạn chưa có giao dịch nào được thực hiện</p>
            </div>
        )
    } else {
        if (allBuffShare.total == 0) {
            return (
                <div style={{ textAlign: "center", fontSize: 16 }}>
                    <p>Bạn chưa có giao dịch nào được thực hiện</p>
                </div>
            )
        }
        return (
            <div className="panel-body">
                <div className="overflow-hidden" style={{ clear: "both" }}>
                    <BootstrapTable
                        onTableChange={onTableChange}
                        keyField='_id'
                        remote={true}
                        data={allBuffShare.length != 0 ? allBuffShare.rows : []}
                        columns={columns}
                        pagination={paginationFactory(paginateOptions)}
                        keyBoardNav
                    />
                </div>
            </div>
        )
    }
}

function PanelForm(props) {

    const [idPost, setIdPost] = useState("");
    const [number, setNumber] = useState(10);
    const [note, setNote] = useState("");
    const [total, setTotal] = useState(4000);
    const { register, handleSubmit, formState: { errors }, reset } = useForm();

    useEffect(() => {
        setIdPost("");
        setNumber(10);
        setNote("");
        setTotal(4000);
        reset({
            id_post: "",
            number: 10,
            note: ""
        });
    }, [props.buffShare])

    function onSubmit(data, e) {
        var idPostProcessed = processIdPost(props, data.post_id);
        if (!idPostProcessed) {
            return;
        }
        props.create({
            post_id: idPostProcessed,
            number: data.number,
            note: data.note
        });
    }

    function handleIdPost(value) {
        var idPostProcessed = processIdPost(props, value);
        if (idPostProcessed) {
            setIdPost(idPostProcessed);
        } else {
            setIdPost(value);
        }
    }

    return (
        <form
            className="form-horizontal"
            onSubmit={handleSubmit(onSubmit)}
        >
            <div className="form-group">
                <label className="col-lg-3 control-label">Link bài viết</label>
                <div className="col-lg-7">
                    <input
                        {...register("post_id", { required: true })}
                        type="text"
                        className="form-control"
                        name="post_id"
                        value={idPost}
                        onChange={e => handleIdPost(e.target.value)}
                        placeholder="Nhập link bài viết công khai"
                    />
                    {errors.post_id?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
                </div>
            </div>
            <div className="form-group">
                <label className="col-lg-3 control-label">Số lượng</label>
                <div className="col-lg-7">
                    <input
                        {...register("number", { required: true, min: 1 })}
                        type="number"
                        className="form-control"
                        name="number"
                        value={number}
                        onChange={e => { setNumber(e.target.value); setTotal(e.target.value * 400) }}
                    />
                    {errors.number?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
                    {errors.number?.type === "min" && <ErrorInput msg="Số lượng tối thiểu là 1." />}
                </div>
            </div>
            <div className="form-group">
                <label className="col-lg-3 control-label">Ghi chú </label>
                <div className="col-lg-7">
                    <input
                        {...register("note")}
                        type="text"
                        className="form-control"
                        name="note"
                        placeholder="Ghi chú về đơn hàng của bạn."
                        value={note}
                        onChange={e => setNote(e.target.value)}
                    />
                </div>
            </div>
            <div className="form-group">
                <div className="col-lg-7 col-lg-offset-3">
                    <div
                        className="alert alert-primary text-center"
                        style={{ fontSize: 16 }}
                    >
                        <span>
                            Tổng: <strong id="totalCoin">{total} nCoin</strong>
                        </span>
                        <br />
                        <span>
                            Gía buff là <strong>400 nCoin</strong>/like
                        </span>
                    </div>
                </div>
            </div>
            <div className="panel-footer clearfix">
                <div className="col-lg-7 col-lg-offset-3 text-center">
                    <button type="submit" className="btn btn-mint">
                        Thanh toán
                    </button>
                </div>
            </div>
        </form>
    )
}

function BuffShare(props) {

    useEffect(()=>{
        props.setPageTitle("Buff like bài viết","Buff like bài viết")
    },[])

    return (
        <div id="page-content">
            <div className="col-md-8">
                <div className="panel">
                    <div className="panel-heading" style={{ borderBottom: "1px solid #eee" }}>
                        <div className="panel-control" style={{ float: "left" }}>
                            <ul className="nav nav-tabs">
                                <li className="active">
                                    <a data-toggle="tab" href="#demo-tabs-box-1">
                                        Thêm ID Buff
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#demo-tabs-box-2">
                                        Danh sách Buff
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="panel-body">
                        <div className="tab-content">
                            <div id="demo-tabs-box-1" className="tab-pane fade in active">
                                <PanelForm {...props} />
                            </div>
                            <div id="demo-tabs-box-2" className="tab-pane fade">
                                <PanelTable {...props} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-md-4">
                <BuffUserCoin />
            </div>
            <div className="col-md-4">
                <PanelNote {...props} />
            </div>
        </div>
    )
}

function mapDispatchToProps(dispatch) {
    return {
        getAll: (filters) => dispatch({ type: GET_BUFF_SHARE_ALL, filters }),
        create: (attributes) => dispatch({ type: CREATE_BUFF_SHARE, attributes }),
        showAlert: (content, variant) => dispatch({ type: ALERT, info: { content: content, variant: variant } }),
        setPageTitle: (main,breadcrumb_1,breadcrumb_2) =>dispatch(setPageTitle(main,breadcrumb_1,breadcrumb_2))
    }
}

function mapStateToProps(state) {
    return {
        allBuffShare: state.buffShare.allBuffShare,
        buffShare: state.buffShare.data
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BuffShare)