import React, { useState, useEffect } from 'react';
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from 'react-bootstrap-table2-paginator';
import { connect } from "react-redux";
import { BuffUserCoin, ErrorInput } from "../../components";
import { CREATE_BUFF_LIVE_STREAM, GET_BUFF_LIVE_STREAM_ALL } from "../../config/ActionTypes/BuffLiveStream";
import { useForm } from "react-hook-form";
import { TIME_LIVESTREAM, PRICE } from "../../config/Constant";
import { ALERT } from '../../config/ActionTypes/Alert';
import { setPageTitle } from '../../actions/page-title.action';

function PanelNote() {
    return (
        <div className="panel">
            <div className="list-group bg-trans">
                <div className="list-group-item">
                    <div className="alert alert-primary" style={{ fontSize: 16 }}>
                        <strong>Thông tin</strong>
                        <br />
                        <small>
                            - Chỉ tăng mắt cho video cho video công khai (nếu cố tình buff
                            video không công khai có thể không lên đủ hoặc không lên).
                            <br />
                            - Video đã buff ở starnhat rồi mà buff thêm bên khác nếu bị tụt
                            mắt sẽ không được xử lý.
                            <br />- Các đơn Buff sẽ không được hoàn tiền với bất kì lý do gì,
                            cân nhắc trước khi Order.
                        </small>
                    </div>
                    <div className="alert alert-danger" style={{ fontSize: 16 }}>
                        <strong>Lưu ý</strong>
                        <br />
                        <small>
                            - Ngiêm cấm Buff các ID Seeding có nội dung vi phạm pháp luật,
                            chính trị, đồi trụy... Nếu cố tình buff bạn sẽ bị trừ hết tiền và
                            banned khỏi hệ thống vĩnh viễn, và phải chịu hoàn toàn trách nhiệm
                            trước pháp luật.
                        </small>
                    </div>
                </div>
            </div>
        </div>
    )
}

function PanelTable(props) {

    const [page, setPage] = useState(1);
    const [sizePerPage, setSizePerPage] = useState(10);
    const [sort, setSort] = useState('created_at');
    const [order, setOrder] = useState('desc');
    const allBuffLivestream = props.allBuffLivestream;

    useEffect(() => {
        props.getAll({
            limit: sizePerPage,
            offset: 0,
            sort: sort,
            order: order,
        });
    }, [props.buffLivestream])

    function onTableChange(type, newState) {
        props.getAll({
            sort: newState.sortField,
            order: newState.sortOrder,
            limit: newState.sizePerPage,
            offset: (newState.page - 1) * newState.sizePerPage,
        });
        setSort(newState.sortField);
        setOrder(newState.sortOrder);
        setPage(newState.page);
        setSizePerPage(newState.sizePerPage);
    }

    const paginateOptions = {
        page: page,
        sizePerPage: sizePerPage,
        showTotal: true,
        totalSize: allBuffLivestream.length != 0 ? allBuffLivestream.total : 0,
        hidePageListOnlyOnePage:true,
        paginationTotalRenderer:(from, to, size)=>{
            return(
                <span className="react-bootstrap-table-pagination-total"> Hiển thị từ { from } đến { to } của { size }</span>
            )
        }
    };

    const columns = [{
        dataField: 'link',
        text: 'Link Livestream',
        sort: true
    }, {
        dataField: 'num_view',
        text: 'SL mắt',
        sort: true
    }, {
        dataField: 'num_comment',
        text: 'SL comment',
        sort: true
    }, {
        dataField: 'num_reaction',
        text: 'SL reaction',
        sort: true
    },
        , {
        dataField: 'status',
        text: 'Trạng thái',
        sort: true,
        headerAlign: 'center',
        align: "center"
    }];

    if (allBuffLivestream.length == 0) {
        return (
            <div style={{ textAlign: "center" }}>
                <p>Bạn chưa có giao dịch nào được thực hiện</p>
            </div>
        )
    } else {
        if (allBuffLivestream.total == 0) {
            return (
                <div style={{ textAlign: "center", fontSize: 16 }}>
                    <p>Bạn chưa có giao dịch nào được thực hiện</p>
                </div>
            )
        }
        return (
            <div className="panel-body">
                <div className="overflow-hidden" style={{ clear: "both" }}>
                    <BootstrapTable
                        onTableChange={onTableChange}
                        keyField='_id'
                        remote={true}
                        data={allBuffLivestream.length != 0 ? allBuffLivestream.rows : []}
                        columns={columns}
                        pagination={paginationFactory(paginateOptions)}
                        keyBoardNav
                    />
                </div>
            </div>
        )
    }
}

function Select(props) {

    return (
        <div className="form-group">
            <label className="col-lg-3 control-label">
                Số phút xem
            </label>
            <div className="col-lg-7">
                <select {...props.register("time_view", { required: true, min: 1 })} name="time_view" className={props.className} value={props.timeView} onChange={e => props.setTimeView(e.target.value)}>
                    {props.data.map((item, index) => {
                        if (item.value == props.selected) {
                            return <option key={index} value={item.value} selected>{item.label}</option>
                        }
                        return <option key={index} value={item.value}>{item.label}</option>
                    })}
                </select>
            </div>
            {props.errors.time_view?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
        </div>
    )
}

function FormBuff(props) {

    const [link, setLink] = useState("");
    const [numView, setNumView] = useState(10);
    const [note, setNote] = useState("");
    const [numComment, setNumComment] = useState(0);
    const [numReaction, setNumReaction] = useState(0);
    const [timeView, setTimeView] = useState(30);
    const { register, handleSubmit, formState: { errors }, reset } = useForm({
        link: "",
        num_view: 30,
        time_view: 30,
        num_comment: 0,
        num_reaction: 0,
        comments: "",
        note: ""
    });

    useEffect(() => {
        setLink("");
        setNumView(30);
        setNote("");
        setTimeView(30);
        setNumComment(0);
        setNumReaction(0);

        reset({
            link: "",
            num_view: 30,
            time_view: 30,
            num_comment: 0,
            num_reaction: 0,
            comments: "",
            note: ""
        });
    }, [props.buffLivestream])

    function onSubmit(data, e) {
        props.create({
            link: data.link,
            num_view: data.num_view,
            time_view: data.time_view,
            num_comment: data.num_comment,
            num_reaction: data.num_reaction,
            comments: data.comments?data.comments:"",
            note: data.note
        });
    }

    return (
        <form
            className="form-horizontal"
            onSubmit={handleSubmit(onSubmit)}
        >
            <div className="form-group">
                <label className="col-lg-3 control-label">
                    Link Livestream
                </label>
                <div className="col-lg-7">
                    <input
                        {...register("link", { required: true })}
                        type="text"
                        className="form-control"
                        name="link"
                        value={link}
                        onChange={e => setLink(e.target.value)}
                        placeholder="Nhập link bài viết công khai"
                    />
                    {errors.link?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
                </div>
            </div>
            <div className="form-group">
                <label className="col-lg-3 control-label">Số lượng mắt</label>
                <div className="col-lg-7">
                    <input
                        {...register("num_view", { required: true, min: 30 })}
                        type="number"
                        className="form-control"
                        value={numView}
                        onChange={e => { setNumView(e.target.value) }}
                    />
                    {errors.num_view?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
                    {errors.num_view?.type === "min" && <ErrorInput msg="Số lượng tối thiểu là 1." />}

                    <div className="alert alert-info" style={{ marginTop: 10 }}>
                        Tổng tiền buff mắt view = (Số lượng) x (Số phút) x (Giá
                        buff)
                    </div>
                </div>
            </div>
            <Select
                timeView={timeView}
                setTimeView={setTimeView}
                errors={errors}
                register={register}
                data={TIME_LIVESTREAM}
                className="form-control col-lg-7"
            />
            <div className="form-group">
                <label className="col-lg-3 control-label">
                    Số lượng comment
                </label>
                <div className="col-lg-7">
                    <input
                        {...register("num_comment", { required: true, min: 0 })}
                        type="number"
                        className="form-control"
                        onchange="changeTotal()"
                        placeholder="Nhập số comment mong muốn."
                        value={numComment}
                        onChange={e => setNumComment(e.target.value)}
                    />
                    {errors.num_comment?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
                    {errors.num_comment?.type === "min" && <ErrorInput msg="Số lượng tối thiểu là 0." />}
                </div>
            </div>
            {
                numComment > 0 &&
                (
                    <div className="form-group">
                        <label className="col-lg-3 control-label">Nội dung</label>
                        <div className="col-lg-7">
                            <textarea
                                {...register("comments", { required: true, min: 0 })}
                                className="form-control"
                                name="comments"
                                placeholder="Nhập nội dung bạn muốn tăng bình luận, mỗi nội dung 1 dòng."
                                rows={10}
                            />
                            {errors.comments?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
                        </div>
                    </div>
                )
            }

            <div className="form-group">
                <label className="col-lg-3 control-label">
                    Số lượng reaction
                </label>
                <div className="col-lg-7">
                    <input
                        {...register("num_reaction", { required: true, min: 0 })}
                        type="number"
                        className="form-control"
                        placeholder="Nhập số reaction mong muốn."
                        value={numReaction}
                        onChange={e=>setNumReaction(e.target.value)}
                    />
                    {errors.num_reaction?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
                    {errors.num_reaction?.type === "min" && <ErrorInput msg="Số lượng tối thiểu là 0." />}
                    <div
                        className="reaction"
                        style={{ fontSize: 50, textAlign: "center" }}
                    >
                        👍🥰😁😂
                    </div>
                </div>
            </div>
            <div className="form-group">
                <label className="col-lg-3 control-label">Ghi chú </label>
                <div className="col-lg-7">
                    <input
                        {...register("note")}
                        type="text"
                        className="form-control"
                        name="note"
                        placeholder="Ghi chú về đơn hàng của bạn."
                        value={note}
                        onChange={e => setNote(e.target.value)}
                    />
                </div>
            </div>
            <div className="form-group">
                <div className="col-lg-7 col-lg-offset-3">
                    <div
                        className="alert alert-primary text-center"
                        style={{ fontSize: 16 }}
                    >
                        <span>
                            Tổng: <strong id="totalCoin">{ numView * timeView * PRICE.PRICE_VIEW + numComment*PRICE.PRICE_COMMENT + numReaction*PRICE.PRICE_COMMENT} nCoin</strong>
                        </span>
                        <br />
                        <span>Gía buff:</span>
                        <br />
                        <span>
                            <strong>{PRICE.PRICE_VIEW} nCoin</strong>/mắt/phút
                        </span>
                        <br />
                        <span>
                            <strong>{PRICE.PRICE_COMMENT} nCoin</strong>/comment
                        </span>
                        <br />
                        <span>
                            <strong>{PRICE.PRICE_REACTION} nCoin</strong>/reaction
                        </span>
                    </div>
                </div>
            </div>
            <div className="panel-footer clearfix">
                <div className="col-lg-7 col-lg-offset-3 text-center">
                    <button
                        type="submit"
                        className="btn btn-mint"
                        name="signup"
                        value="Sign up"
                    >
                        Thanh toán
                    </button>
                </div>
            </div>
        </form>
    )
}

function BuffLivestream(props) {

    useEffect(()=>{
        props.setPageTitle("Buff Livestream","Buff Livestream");
    },[])

    return (
        <div id="page-content">
            <div className="col-md-8">
                <div className="panel">
                    <div className="panel-heading" style={{ borderBottom: "1px solid #eee" }}>
                        <div className="panel-control" style={{ float: "left" }}>
                            <ul className="nav nav-tabs">
                                <li className="active">
                                    <a data-toggle="tab" href="#demo-tabs-box-1">
                                        Thêm ID Buff
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#demo-tabs-box-2">
                                        Danh sách Buff
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="panel-body">
                        <div className="tab-content">
                            <div id="demo-tabs-box-1" className="tab-pane fade in active">
                                <FormBuff {...props} />
                            </div>
                            <div id="demo-tabs-box-2" className="tab-pane fade">
                                <PanelTable {...props} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-md-4">
                <BuffUserCoin />
            </div>
            <div className="col-md-4">
                <PanelNote {...props} />
            </div>
        </div>
    )
}

function mapDispatchToProps(dispatch) {
    return {
        getAll: (filters) => dispatch({ type: GET_BUFF_LIVE_STREAM_ALL, filters }),
        create: (attributes) => dispatch({ type: CREATE_BUFF_LIVE_STREAM, attributes }),
        showAlert: (content, variant) => dispatch({ type: ALERT, info: { content: content, variant: variant } }),
        setPageTitle: (main,breadcrumb_1,breadcrumb_2) =>dispatch(setPageTitle(main,breadcrumb_1,breadcrumb_2))
    }
}

function mapStateToProps(state) {
    return {
        allBuffLivestream: state.buffLivestream.allBuffLivestream,
        buffLivestream: state.buffLivestream.data
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BuffLivestream)