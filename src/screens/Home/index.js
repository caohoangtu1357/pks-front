import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { CLEAN_POSTS, LOAD_NOTIFY } from '../../config/ActionTypes/Notify';
import { LOAD_MORE_POST } from '../../config/ActionTypes/Post';
import { URL_STATIC_POST } from "../../config/Constant";
import { setPageTitle } from '../../actions/page-title.action';
import { convertNumberToK, numberWithDot } from '../../config/General';

function PanelReport(props) {
  return (
    <div className="panel">
      <div className="panel-heading">
        <h3 className="panel-title">Báo cáo</h3>
      </div>
      <div className="list-group bg-trans">
        <div className="row">
          <div className="col-sm-6">
            <div className="media media-info">
              <div className="media-left follow">
                <i className="fa fa-rss text-main icon-2x" />
              </div>
              <div className="media-body">
                <p className="text-main text-lg mar-no">0</p>
                Follow
              </div>
            </div>
          </div>
          <div className="col-sm-6">
            <div className="media media-info">
              <div className="media-left like-page">
                <i className="fa fa-thumbs-up icon-lg text-main icon-2x" />
              </div>
              <div className="media-body">
                <p className="text-main text-lg mar-no">0</p>
                Like Page
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-6">
            <div className="media media-info">
              <div className="media-left nap-thang">
                <img
                  src="assets/images/coins-solid.svg"
                  style={{ width: 25 }}
                  className="text-main icon-2x"
                />
              </div>
              <div className="media-body">
                <p className="text-main text-lg mar-no">{numberWithDot(props.recharge.total_amount_current_month)}</p>
                Nạp tháng
              </div>
            </div>
          </div>
          <div className="col-sm-6">
            <div className="media media-info">
              <div className="media-left giam-0">
                <img
                  src="assets/images/crown-solid.svg"
                  style={{ width: 25 }}
                  className="text-main icon-2x"
                />
              </div>
              <div className="media-body">
                <p className="text-main text-lg mar-no">Giảm 0%</p>
                Thành viên
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}


function PanelNotifies(props) {

  const notifies = props.notifies;

  useEffect(() => {
    props.loadNotify();
  }, []);

  return (
    <div className="panel">
      <div className="panel-heading">
        <h3 className="panel-title">Thông báo mới</h3>
      </div>
      <div className="list-group bg-trans">
        {
          notifies.map((notify, index) => {
            return (
              <a href="#" className="list-group-item" key={index}>
                <div className="media-left pos-rel">
                  <img
                    className="img-circle img-xs"
                    src={URL_STATIC_POST + notify.image_path.replace("public", "storage")}
                  />
                </div>
                <div className="media-body" style={{ wordBreak: "break-all" }}>
                  {<div className="mar-no" dangerouslySetInnerHTML={{ __html: (notify.content) }} />}
                </div>
              </a>
            )
          })
        }
      </div>
    </div>
  )
}

function PanelUser(props) {
  return (
    <div className="panel">
      <div className="panel-body" style={{ paddingBottom: 15 }}>
        <div className="media">
          <a className="media-left" href="#">
            <img
              className="img-circle img-xs"
              src="img/profile-photos/1.png"
            />
          </a>
          <div className="media-body" style={{ fontSize: "1.6rem" }}>
            <div>
              <a
                href="#"
                className="btn-link text-semibold media-heading box-inline"
              >
                {props.user.full_name} (Thành viên)
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

function PanelBalance(props) {
  return (
    <div className="row demo-nifty-panel">
      <div className="col-sm-6">
        <div className="panel panel-colorful panel-primary">
          <div className="panel-heading">
            <h3 className="panel-title">Số dư hiện tại</h3>
          </div>
          <div className="panel-body panel-balance">
            <p>{numberWithDot(props.user.balance)} nCoin</p>
          </div>
        </div>
      </div>
      <div className="col-sm-6">
        <div className="panel panel-colorful panel-info">
          <div className="panel-heading">
            <h3 className="panel-title">Tổng tiền nạp</h3>
          </div>
          <div className="panel-body panel-balance">
            <p>{numberWithDot(props.recharge.total)} nCoin</p>
          </div>
        </div>
      </div>
    </div>
  )
}

function Comments(props) {
  const comments = props.comments;
  const total = props.total;

  if (comments.length > 0) {
    return (
      <>
        {
          comments.map((comment, index) => {
            return (
              <div className="pad-top" key={index}>
                <div className="media">
                  <a
                    className="media-left"
                    href="${item.facebook_link?item.facebook_link:'#'}"
                    target="_blank"
                  >
                    <img
                      className="img-circle img-xs"
                      src={URL_STATIC_POST + comment.image_path.replace("public", "storage")}
                    />
                  </a>
                  <div className="media-body">
                    <div className="comment-info">
                      <div>
                        <a
                          href={comment.facebook_link ? comment.facebook_link : '#'}
                          target="_blank"
                          className="btn-link text-semibold media-heading box-inline"
                        >
                          {comment.name} <i className="fa fa-check-circle" />
                        </a>
                        <small className="text-muted pad-lft" />
                      </div>
                      {comment.content}
                    </div>
                    <div className="media-body">
                      <div className="reaction-action">
                        <a href="#">Thích</a> • <a href="#">Trả lời</a> • 22 phút{" "}
                      </div>
                    </div>
                  </div>
                </div>
                <button className="btn btn-trans">
                  <span className="text-semibold" />
                  Tải thêm bình luận {comments.length + " của " + numberWithDot(total)}
                </button>
              </div>
            )
          })
        }
      </>
    )
  } else {
    return <div className="hide"></div>;
  }
}

function MediaContent(props) {
  const post = props.post;
  switch (post.type) {
    case 'image':
      var imagePath = URL_STATIC_POST + post.image_path.replace("public", "storage");
      return (
        <img className="img-responsive" src={imagePath} alt="Image" />
      )
    case 'video':
      var videoPath = URL_STATIC_POST + post.video_path.replace("public", "storage");
      return (
        <video style="width: 100%;" controls>
          <source id={post._id} src={videoPath} type="video/mp4" />
          Trình duyệt không hổ trợ
        </video>
      )
    default:
      return (
        <></>
      );
  }
}

function MainContent(props) {

  const posts = props.posts;

  useEffect(() => {
    props.loadMorePost({
      offset: 0
    });
    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    }
  }, []);

  function handleScroll(e) {
    if (document.documentElement.scrollTop == document.body.scrollHeight - window.innerHeight) {
      props.loadMorePost({
        offset: document.getElementsByClassName("post-timeline-item").length,
      });
    }
  }

  var curDate = new Date();
  var datePost = curDate.getDate() + "/" + (curDate.getMonth() + 1) + "/" + curDate.getFullYear();
  curDate = new Date(curDate.setHours(curDate.getHours() - 1));
  var hourPost = curDate.getHours() + ":" + curDate.getMinutes();

  return (
    <div>
      {
        posts.map(function (post, index) {
          var lastUserComment = "";
          var numComments = post.comments.length > 0
          if (numComments > 0) {
            lastUserComment = post.comments[numComments - 1].name;
          }
          return (
            <div className="panel post-timeline-item" key={index}>
              <div className="panel-body">
                <div className="post-loaded media-block">
                  <a className="media-left" href="#">
                    <img
                      className="img-circle img-sm"
                      src="img/profile-photos/10.png"
                    />
                  </a>
                  <div className="pad-btm">
                    <a className="media-left" href="#"></a>
                    <a
                      href="#"
                      className="btn-link text-semibold media-heading box-inline"
                      style={{ fontSize: "1.5rem" }}
                    >
                      Quản trị viên <i className="fa fa-check-circle" />
                    </a>
                    <p className="text-muted text-sm" style={{ fontWeight: 400 }}>
                      {hourPost} • {datePost} •{" "}
                      <i className="fa fa-globe" />
                    </p>
                  </div>
                  <div className="media-body">
                    <div style={{ wordWrap: "break-word", whiteSpace: "break-spaces" }}>
                      {<div dangerouslySetInnerHTML={{ __html: (post.content) }} />}
                    </div>
                    <MediaContent post={post} />
                    <div className="media">
                      <div className="media-left">
                        <ul className="list-inline">
                          <li>
                            <i
                              className="fa fa-lg fa-heart"
                              style={{ color: "#3f89ecc9" }}
                            />
                            <span className="text-semibold num-interaction">
                              {
                                (lastUserComment == '' ? '' : ' ' + lastUserComment + " và") + (post.num_like == 0 ? " 0 Thích " : " " + convertNumberToK(post.num_like) + " người khác")
                              }
                            </span>
                          </li>
                          <li>
                            <i
                              className="fa fa-lg fa-comment"
                              style={{ color: "#3f89ecc9" }}
                            />
                            <span className="text-semibold num-interaction">
                              {
                                (post.num_comment == 0 ? " " + 0 : " " + convertNumberToK(post.num_comment)) + " Bình luận"
                              }
                            </span>
                          </li>
                        </ul>
                      </div>
                      <div className="media-right">
                        <ul className="list-inline mar-no">
                          <li>
                            <a href="#" className="text-bold text-sm">
                              <i className="fa fa-lg fa-share" />
                              {
                                (post.num_share == 0 ? " " + 0 : " " + convertNumberToK(post.num_share)) + " Chia sẻ"
                              }
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="media pad-btm pad-top bord-top">
                      <a className="media-left" href="#">
                        <img
                          className="img-circle img-xs"
                          src="/img/profile-photos/1.png"
                        />
                      </a>
                      <div className="media-body">
                        <input
                          type="text"
                          className="form-control comment-input"
                          placeholder="Viết bình luận công khai"
                          readOnly
                        />
                      </div>
                    </div>
                    <Comments comments={post.comments} total={post.num_comment} />
                  </div>
                </div>
              </div>
            </div>
          )
        })
      }
    </div>
  )
}

function Home(props) {

  useEffect(() => {
    props.setTitle("Trang chủ", "Trang chủ");
    return () => {
      props.cleanPosts();
    }
  }, []);

  return (
    <div id="page-content">
      <div className="col-md-7">
        <PanelUser user={props.user} />
        <PanelBalance user={props.user} recharge={props.recharge} />
        <MainContent {...props} />
      </div>
      <div className="col-md-4">
        <PanelReport recharge={props.recharge} />
        <PanelNotifies {...props} />
      </div>
    </div>
  )
}

function mapStateToProps(state) {
  return {
    user: state.user.data,
    recharge: state.recharge.data,
    posts: state.post.posts,
    postsOffset: state.post.postsOffset,
    notifies: state.notify.notifies
  }
}

function mapDispatchToProps(dispatch) {
  return {
    loadMorePost: (filters) => dispatch({ type: LOAD_MORE_POST, filters: filters }),
    loadNotify: () => dispatch({ type: LOAD_NOTIFY }),
    cleanPosts: () => dispatch({ type: CLEAN_POSTS }),
    setTitle: (main, breadcrumb_1, breadcrumb_2) => dispatch(setPageTitle(main, breadcrumb_1, breadcrumb_2))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);