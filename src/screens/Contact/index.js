import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import { DELETE_CONTACT, GET_CONTACT_ALL, STATISTIC_CONTACT } from '../../config/ActionTypes/Contact';
import './styleIndex.scss';
import { CONTACT_STATUS } from "../../config/Constant";
import Swal from "../../config/Swal/index";
import { setPageTitle } from '../../actions/page-title.action';

function PanelNote(props) {
    return (
        <div id="demo-panel-w-alert" className="panel">
            <div className="panel-alert">
                <div className="alert-wrap in">
                    <div className="alert-success-custom alert" role="alert">
                        <button className="close" type="button">
                            <i className="pci-cross pci-circle" />
                        </button>
                        <div className="media">
                            <h4 className="panel-header">Lưu ý!</h4>
                            <p>
                                - Hỗ trợ trong giờ hành chính từ Thứ 2 đến Thứ 6.
                                <br />
                                - Giờ làm việc từ 9h30 sáng đến 18h30 chiều.
                                <br />
                                - Ngoài giờ làm việc sẽ hỗ trợ chậm hơn và phụ thuộc nhân viên
                                hỗ trợ Online.
                                <br />
                                - Nếu vấn đề không cần gấp vui lòng chờ đến giờ làm việc để xử
                                lý tốt nhất, nhường cho các bạn cần hỗ trợ gấp ngoài giờ.
                                <br />
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

function ActionColumn(props) {

    function deleteBtn(id) {
        Swal({
            title:"Xóa",
            text:"Xác nhận xóa liên hệ hỗ trợ",
            icon:"warning",
            hadleAction:()=>{
                props.deleteContact(id);
                props.getAll({
                    limit: props.sizePerPage,
                    offset: 0,
                    sort: 'created_at',
                    order: 'desc',
                });
            }
        });
    }

    return (
        <div key={props.id}>
            <Link
                to={"/contact/update/"+props.id}
                className="add-tooltip btn btn-primary btn-xs"
                data-placement="top"
                title="Chỉnh sửa Liên Hệ Hỗ Trợ"
            >
                <i className="fa fa-edit" />
            </Link>
            <button
                onClick={(e) => deleteBtn(e.currentTarget.value)}
                style={{ marginLeft: 5 }}
                value={props.id}
                className="add-tooltip btn btn-danger btn-xs cancel-btn"
                data-placement="top"
                title="Xoá liên hệ hỗ trợ"
            >
                <i className="fa fa-trash" />
            </button>
        </div>
    )
}

function StatusColumn(props) {
    if (props.status == 0) {
        return <span className="label label-table label-primary contact-status-label">{CONTACT_STATUS[0].label}</span>
    }
    if (props.status == 1) {
        return <span class="label label-table label-warning contact-status-label">{CONTACT_STATUS[1].label}</span>
    }
    if (props.status == 2) {
        return <span class="label label-table label-success contact-status-label">{CONTACT_STATUS[2].label}</span>
    }
}

function PanelTable(props) {

    const [page, setPage] = useState(1);
    const [sizePerPage, setSizePerPage] = useState(10);
    const [status, setStatus] = useState(null);
    const [sort,setSort] = useState('created_at');
    const [order,setOrder] = useState('desc');

    useEffect(() => {
        props.getAll({
            limit: sizePerPage,
            offset: 0,
            sort: sort,
            order: order,
            status: status
        });
    }, [])

    function onTableChange(type, newState) {
        props.getAll({
            sort: newState.sortField,
            order: newState.sortOrder,
            limit: newState.sizePerPage,
            offset: (newState.page - 1) * newState.sizePerPage,
            status: status
        });
        setSort(newState.sortField);
        setOrder(newState.sortOrder);
        setPage(newState.page);
        setSizePerPage(newState.sizePerPage);
    }

    function handleFilterStatus(){
        props.getAll({
            limit: sizePerPage,
            offset: null,
            sort: sort,
            order: order,
            status: parseInt(status)
        });
    }

    const paginateOptions = {
        page: page,
        sizePerPage: sizePerPage,
        showTotal: true,
        totalSize: props.allContact.length != 0 ? props.allContact.total : 0,
        hidePageListOnlyOnePage:true,
        paginationTotalRenderer:(from, to, size)=>{
            return(
                <span className="react-bootstrap-table-pagination-total"> Hiển thị từ { from } đến { to } của { size }</span>
            )
        }
    };

    const columns = [{
        dataField: 'created_at',
        text: 'STT',
        headerAlign: 'center',
        align: "center",
        style: { width: 50 },
        headerStyle: { width: 50 },
        formatter: (cell, row, index) => (index + 1)
    }, {
        dataField: 'problem',
        text: 'Vấn đề',
        sort: true
    }, {
        dataField: 'title',
        text: 'Tiêu đề',
        sort: true
    }, {
        dataField: 'status',
        text: 'Trạng thái',
        headerAlign: 'center',
        align: "center",
        sort: true,
        formatter: (cell, row, index) => (<StatusColumn status={row.status} />)
    }, {
        dataField: '_id',
        text: 'Chức năng',
        headerAlign: 'center',
        align: "center",
        formatter: (cell, row, index) => (<ActionColumn id={row._id} sizePerPage={sizePerPage} {...props} />)
    }];

    if (props.allContact.length == 0) {
        return (
            <div style={{ textAlign: "center" }}>
                <p>Loading</p>
            </div>
        )
    } else {
        if (props.allContact == 0) {
            return (
                <div style={{ textAlign: "center" ,fontSize:16}}>
                    <p>Bạn chưa có liên hệ hỗ trợ.</p>
                </div>
            )
        }

        return (
            <>
                <div className="pull-left">
                    <h3 className="text-main">Hỗ trợ của bạn</h3>
                </div>
                <div
                    className="pull-right"
                    style={{ minWidth: "50%", textAlign: "right" }}
                >
                    <div className="row">
                        <div className="col-md-5">
                            <Select 
                                setStatus={setStatus} 
                                handleFilterStatus={handleFilterStatus} 
                                className="form-control" 
                                data={[{ value: "", label: "Tất cả" }, ...CONTACT_STATUS]} {...props} 
                            />
                        </div>
                        <div className="col-md-2">

                        </div>
                        <div className="col-md-5">
                            <div className="form-group">
                                <Link to="/contact/create">
                                    <button className="btn btn-info">
                                        <i className="fa fa-plus" />
                                        Tạo hỗ trợ
                                    </button>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="overflow-hidden" style={{ clear: "both" }}>
                    <BootstrapTable
                        onTableChange={onTableChange}
                        keyField='_id'
                        remote={true}
                        data={props.allContact.length != 0 ? props.allContact.rows : []}
                        columns={columns}
                        pagination={paginationFactory(paginateOptions)}
                        keyBoardNav
                    />

                </div>
            </>
        )
    }
}

function Select(props) {

    function handleFilterStatus(status) {
        props.setStatus(status);
    }

    return (
        <div className="form-group" style={{ display: 'flex' }}>
            <select name={props.name} className={props.className} value={props.status} onChange={e => handleFilterStatus(e.target.value)}>
                {props.data.map((item, index) => {
                    if (item.value == props.selected) {
                        return <option key={index} value={item.value} selected>{item.label}</option>
                    }
                    return <option key={index} value={item.value}>{item.label}</option>
                })}
            </select>
            <button className="btn btn-warning" id="refreshSearch" onClick={props.handleFilterStatus}>
                <i className="fa fa-recycle" />
            </button>
            <label
                id="problem-error"
                className="error"
                htmlFor="problem"
                style={{ display: "none" }}
            />
        </div>
    )
}

function Contact(props) {

    useEffect(()=>{
        props.setPageTitle("Liên hệ hỗ trợ","Liên hệ hỗ trợ");
        props.statisticContact();
    },[props.contact]);

    const statistic = props.statistic;

    return (
        <div id="page-content">
            <div className="row">
                <PanelNote {...props} />
            </div>
            <div className="row">
                <div className="panel">
                    <div className="panel-body">
                        <div className="col-md-4">
                            <div className="panel media middle pad-all">
                                <div className="media-left">
                                    <span className="icon-wrap icon-wrap-sm icon-circle bg-success">
                                        <i className="demo-pli-clock icon-2x" />
                                    </span>
                                </div>
                                <div className="media-body">
                                    <p className="text-muted mar-no">Chờ hỗ trợ</p>
                                    <p className="text-2x mar-no text-semibold">{statistic.waiting}</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="panel media middle pad-all">
                                <div className="media-left">
                                    <span className="icon-wrap icon-wrap-sm icon-circle bg-success">
                                        <i className="demo-pli-support icon-2x" />
                                    </span>
                                </div>
                                <div className="media-body">
                                    <p className="text-muted mar-no">Đang hổ trợ</p>
                                    <p className="text-2x mar-no text-semibold">{statistic.processing}</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="panel media middle pad-all">
                                <div className="media-left">
                                    <span className="icon-wrap icon-wrap-sm icon-circle bg-success">
                                        <i className="demo-pli-check icon-2x" />
                                    </span>
                                </div>
                                <div className="media-body">
                                    <p className="text-muted mar-no">Đã hỗ trợ</p>
                                    <p className="text-2x mar-no text-semibold">{statistic.processed}</p>
                                </div>
                            </div>
                        </div>
                        <div className="row" style={{ padding: 14 }}>
                            <PanelTable {...props} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        allContact: state.contact.allContact,
        contact:state.contact.data,
        statistic:state.contact.statistic
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getAll: (filters) => dispatch({ type: GET_CONTACT_ALL, filters }),
        deleteContact: (id) => dispatch({ type: DELETE_CONTACT, id }),
        statisticContact:() => dispatch({type:STATISTIC_CONTACT}),
        setPageTitle: (main,breadcrumb_1,breadcrumb_2) =>dispatch(setPageTitle(main,breadcrumb_1,breadcrumb_2))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Contact);