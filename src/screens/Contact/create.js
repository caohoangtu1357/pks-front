import { useState, useEffect } from "react"
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { setPageTitle } from "../../actions/page-title.action";
import { CREATE_CONTACT } from "../../config/ActionTypes/Contact";
import { contactProblems } from "../../config/Constant";
import { useForm } from "react-hook-form";
import "./styleCreate.scss";

function PanelNote() {
    return (
        <div className="panel panel-note-contact">
            <div className="panel-alert">
                <div className="alert-wrap in">
                    <div className="alert-success-custom alert" role="alert">
                        <div className="media">
                            <h4 className="panel-header">Quan trọng!</h4>
                            <p>
                                - Hỗ trợ trong giờ hành chính từ Thứ 2 đến Thứ 6.
                                <br />
                                - Giờ làm việc từ 9h30 sáng đến 18h30 chiều.
                                <br />
                                - Ngoài giờ làm việc sẽ hỗ trợ chậm hơn và phụ thuộc nhân viên
                                hỗ trợ Online.
                                <br />
                                - Nếu vấn đề không cần gấp vui lòng chờ đến giờ làm việc để xử
                                lý tốt nhất, nhường cho các bạn cần hỗ trợ gấp ngoài giờ.
                                <br />
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="panel-alert">
                <div className="alert-wrap in">
                    <div className="alert-danger-custom alert" role="alert">
                        <div className="media">
                            <h4 className="panel-header">Lưu ý!</h4>
                            <p>
                                - Hỗ trợ trong giờ hành chính từ Thứ 2 đến Thứ 6.
                                <br />
                                - Giờ làm việc từ 9h30 sáng đến 18h30 chiều.
                                <br />
                                - Ngoài giờ làm việc sẽ hỗ trợ chậm hơn và phụ thuộc nhân viên
                                hỗ trợ Online.
                                <br />
                                - Nếu vấn đề không cần gấp vui lòng chờ đến giờ làm việc để xử
                                lý tốt nhất, nhường cho các bạn cần hỗ trợ gấp ngoài giờ.
                                <br />
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

function FileInputGroup(props) {

    const [selectedFile, setSelectedFile] = useState();
    const [preview, setPreview] = useState();

    useEffect(() => {
        if (!selectedFile) {
            setPreview(undefined)
            return
        }

        const objectUrl = URL.createObjectURL(selectedFile)
        setPreview(objectUrl)

        return () => URL.revokeObjectURL(objectUrl)
    }, [selectedFile])

    const onSelectFile = e => {
        if (!e.target.files || e.target.files.length === 0) {
            setSelectedFile(undefined)
            return
        }
        props.setImage(e.target.files[0]);
        setSelectedFile(e.target.files[0])
    }


    return (
        <>
            <div
                className="form-group"
                id="img-preview-wrapper"
            >
                <div style={{ width: "100%", textAlign: "center" }}>
                    {selectedFile && <img style={{ maxWidth: 250 }} src={preview} />}
                </div>
            </div>
            <div className="form-group">
                <div className="row">
                    <div className="col-md-12">
                        <span className="pull-left btn btn-success btn-file">
                            <i className="fa fa-upload" /> Thêm hình ảnh
                            <input
                                onChange={onSelectFile}
                                id="image"
                                name="image"
                                type="file"
                            />
                        </span>
                    </div>
                </div>
            </div>
        </>
    )
}


function Select(props) {

    const [problem, setProblem] = useState();

    function handleSelect(e) {
        props.setProblem(e.target.value);
        setProblem(e.target.value);
    }

    return (
        <div className="form-group">
            <label htmlFor="problem" className="control-label">
                Vấn đề cần hỗ trợ
            </label>
            <select name={props.name} className={props.className} value={problem} onChange={e => handleSelect(e)}>
                {props.data.map((item, index) => {
                    if (item.value == props.selected) {
                        return <option key={index} value={item.value} selected>{item.label}</option>
                    }
                    return <option key={index} value={item.value}>{item.label}</option>
                })}
            </select>
            <label
                id="problem-error"
                className="error"
                htmlFor="problem"
                style={{ display: "none" }}
            />
        </div>
    )
}

function CreateContact(props) {

    const [problem, setProblem] = useState("");
    const [title, setTitle] = useState("");
    const [contactId, setContactId] = useState("");
    const [description, setDescription] = useState("");
    const [image, setImage] = useState("");
    const { register, handleSubmit, formState: { errors } ,reset} = useForm();

    useEffect(() => {
        props.setPageTitle("Tạo liên hệ hỗ trợ","Liên hệ hỗ trợ","Tạo mới");
    },[])

    function onSubmit(data,e){
        props.createContact({
            problem: problem,
            title: title,
            contact_id: contactId,
            description: description,
            image: image,
        }, {
            props: props
        })
    }

    return (
        <>
            <div id="page-content">
                <div className="col-md-8">
                    <div className="panel">
                        <div className="panel-body">
                            <div className="panel">
                                <div className="panel-heading">
                                    <h3 className="panel-title text-primary">TẠO HỖ TRỢ MỚI</h3>
                                </div>
                                <form onSubmit={handleSubmit(onSubmit)} encType="multipart/form-data">
                                    <div className="panel-body">
                                        <Select data={contactProblems} setProblem={setProblem} className={"form-control"} />
                                        <div className="form-group">
                                            <label htmlFor="title" className="control-label">
                                                Tiêu đề hỗ trợ
                                            </label>
                                            <input
                                                onChange={e => setTitle(e.target.value)}
                                                value={title}
                                                type="text"
                                                className="form-control"
                                                placeholder="Tiêu đề cần hỗ trợ"
                                            />
                                            <label
                                                id="title-error"
                                                className="error"
                                                htmlFor="title"
                                                style={{ display: "none" }}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="contact_id" className="control-label">
                                                ID nếu có
                                            </label>
                                            <input
                                                onChange={e => setContactId(e.target.value)}
                                                value={contactId}
                                                type="text"
                                                id="contact_id"
                                                name="contact_id"
                                                className="form-control"
                                                placeholder="Nhập ID nếu có"
                                            />
                                            <label
                                                id="contact_id-error"
                                                className="error"
                                                htmlFor="contact_id"
                                                style={{ display: "none" }}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="description" className="control-label">
                                                Mô tả chi tiết
                                            </label>
                                            <textarea
                                                onChange={e => setDescription(e.target.value)}
                                                value={description}
                                                className="form-control"
                                                rows={7}
                                                name="description"
                                                id="description"
                                                placeholder="Mô tả chi tiết của bạn"
                                            />
                                            <label
                                                id="description-error"
                                                className="error"
                                                htmlFor="description"
                                                style={{ display: "none" }}
                                            />
                                        </div>
                                        <FileInputGroup setImage={setImage} />
                                        <div className="form-group">
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <button
                                                        className="btn btn-primary"
                                                        type="submit"
                                                        style={{ width: "100%" }}
                                                    >
                                                        Gửi hỗ trợ
                                                    </button>
                                                </div>
                                                <div className="col-md-6">
                                                    <button
                                                        className="btn btn-danger"
                                                        type="reset"
                                                        style={{ width: "100%" }}
                                                    >
                                                        Hủy bỏ
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-4">
                    <PanelNote />
                </div>
            </div>
        </>
    )
}

function mapStateToProps(state) {
    return {
        contact: state.contact.data
    }
}

function mapDispatchToProps(dispatch) {
    return {
        createContact: (contactInfo, props) => dispatch({ type: CREATE_CONTACT, contactInfo, ...props }),
        setPageTitle: (main,breadcrumb_1,breadcrumb_2) =>dispatch(setPageTitle(main,breadcrumb_1,breadcrumb_2))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(CreateContact));