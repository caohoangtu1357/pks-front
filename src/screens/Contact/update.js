import { useState, useEffect } from "react"
import { connect } from "react-redux";
import { withRouter, useParams } from "react-router-dom";
import { setPageTitle } from "../../actions/page-title.action";
import { GET_CONTACT_DETAIL, UPDATE_CONTACT } from "../../config/ActionTypes/Contact";
import { contactProblems } from "../../config/Constant";
import "./styleCreate.scss";
import { useForm } from "react-hook-form";
import { ErrorInput } from "../../components";
import { getContactImageUrl } from "../../config/General";

function PanelNote() {
    return (
        <div className="panel panel-note-contact">
            <div className="panel-alert">
                <div className="alert-wrap in">
                    <div className="alert-success-custom alert" role="alert">
                        <div className="media">
                            <h4 className="panel-header">Quan trọng!</h4>
                            <p>
                                - Hỗ trợ trong giờ hành chính từ Thứ 2 đến Thứ 6.
                                <br />
                                - Giờ làm việc từ 9h30 sáng đến 18h30 chiều.
                                <br />
                                - Ngoài giờ làm việc sẽ hỗ trợ chậm hơn và phụ thuộc nhân viên
                                hỗ trợ Online.
                                <br />
                                - Nếu vấn đề không cần gấp vui lòng chờ đến giờ làm việc để xử
                                lý tốt nhất, nhường cho các bạn cần hỗ trợ gấp ngoài giờ.
                                <br />
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="panel-alert">
                <div className="alert-wrap in">
                    <div className="alert-danger-custom alert" role="alert">
                        <div className="media">
                            <h4 className="panel-header">Lưu ý!</h4>
                            <p>
                                - Hỗ trợ trong giờ hành chính từ Thứ 2 đến Thứ 6.
                                <br />
                                - Giờ làm việc từ 9h30 sáng đến 18h30 chiều.
                                <br />
                                - Ngoài giờ làm việc sẽ hỗ trợ chậm hơn và phụ thuộc nhân viên
                                hỗ trợ Online.
                                <br />
                                - Nếu vấn đề không cần gấp vui lòng chờ đến giờ làm việc để xử
                                lý tốt nhất, nhường cho các bạn cần hỗ trợ gấp ngoài giờ.
                                <br />
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

function FileInputGroup(props) {

    const [selectedFile, setSelectedFile] = useState();
    const [preview, setPreview] = useState();

    useEffect(() => {
        if (!selectedFile) {
            setPreview(undefined)
            return
        }

        const objectUrl = URL.createObjectURL(selectedFile)
        setPreview(objectUrl)

        return () => URL.revokeObjectURL(objectUrl)
    }, [selectedFile])

    const onSelectFile = e => {
        if (!e.target.files || e.target.files.length === 0) {
            setSelectedFile(undefined)
            return
        }
        props.setImage(e.target.files[0]);
        setSelectedFile(e.target.files[0])
    }

    return (
        <>
            <div
                className="form-group"
                id="img-preview-wrapper"
            >
                <div style={{ width: "100%", textAlign: "center" }}>
                    {selectedFile && <img style={{ maxWidth: 250 }} src={preview} />}
                </div>
            </div>
            <div className="form-group">
                <div style={{ width: "100%", textAlign: "center" }}>
                    {props.oldImage&& !selectedFile && <img style={{ maxWidth: 250 }} src={getContactImageUrl(props.oldImage)} />}
                </div>
            </div>
            <div className="form-group">
                <div className="row">
                    <div className="col-md-12">
                        <span className="pull-left btn btn-success btn-file">
                            <i className="fa fa-upload" /> Chọn ảnh mới
                            <input
                                onChange={onSelectFile}
                                id="image"
                                name="image"
                                type="file"
                            />
                        </span>
                    </div>
                </div>
            </div>
        </>
    )
}


function Select(props) {

    return (
        <div className="form-group">
            <label htmlFor="problem" className="control-label">
                Vấn đề cần hỗ trợ
            </label>
            <select name={props.name} {...props.register("problem", { required: true })} className={props.className} value={props.problem}>
                {props.data.map((item, index) => {
                    if (item.value == props.selected) {
                        return <option key={index} value={item.value} selected>{item.label}</option>
                    }
                    return <option key={index} value={item.value}>{item.label}</option>
                })}
            </select>
            {props.errors.problem?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
        </div>
    )
}

function UpdateContact(props) {

    const [image, setImage] = useState("");
    const id = useParams().id;
    const contact = props.contact;
    const { register, handleSubmit, formState: { errors }, setValue } = useForm();

    setValue("title", contact?.title);
    setValue("problem", contact?.problem);
    setValue("description", contact?.description);
    setValue("contact_id", contact?.contact_id);

    useEffect(() => {
        props.setPageTitle("Cập nhật liên hệ hỗ trợ", "Liên hệ hỗ trợ", "Cập nhật");
        props.getContact(id);
    }, [])

    function onSubmit(data, e) {
        props.updateContact({
            problem: data.problem,
            title: data.title,
            contact_id: data.contact_id,
            description: data.description,
            image: image,
        },
            id,
            props
        );
    }

    return (
        <>
            <div id="page-content">
                <div className="col-md-8">
                    <div className="panel">
                        <div className="panel-body">
                            <div className="panel">
                                <div className="panel-heading">
                                    <h3 className="panel-title text-primary">TẠO HỖ TRỢ MỚI</h3>
                                </div>
                                <form onSubmit={handleSubmit(onSubmit)} encType="multipart/form-data">
                                    <div className="panel-body">
                                        <Select data={contactProblems} register={register} errors={errors} selected={contact?.problem} className={"form-control"} />
                                        <div className="form-group">
                                            <label htmlFor="title" className="control-label">
                                                Tiêu đề hỗ trợ
                                            </label>
                                            <input
                                                {...register("title", { required: true })}
                                                type="text"
                                                className="form-control"
                                                placeholder="Tiêu đề cần hỗ trợ"
                                            />
                                            {errors.title?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="contact_id" className="control-label">
                                                ID nếu có
                                            </label>
                                            <input
                                                {...register("contact_id")}
                                                type="text"
                                                className="form-control"
                                                placeholder="Nhập ID nếu có"
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="description" className="control-label">
                                                Mô tả chi tiết
                                            </label>
                                            <textarea
                                                {...register("description", { required: true })}
                                                className="form-control"
                                                rows={7}
                                                placeholder="Mô tả chi tiết của bạn"
                                            />
                                            {errors.description?.type === "required" && <ErrorInput msg="Phần thông tin bắt buộc." />}
                                        </div>
                                        <FileInputGroup setImage={setImage} oldImage={contact?.image_path}/>
                                        <div className="form-group">
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <button
                                                        className="btn btn-primary"
                                                        type="submit"
                                                        style={{ width: "100%" }}
                                                    >
                                                        Cập nhật hỗ trợ
                                                    </button>
                                                </div>
                                                <div className="col-md-6">
                                                    <button
                                                        className="btn btn-danger"
                                                        type="reset"
                                                        style={{ width: "100%" }}
                                                        onClick={(e)=>props.history.push("/contact")}
                                                    >
                                                        Hủy bỏ
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-4">
                    <PanelNote />
                </div>
            </div>
        </>
    )
}

function mapStateToProps(state) {
    return {
        contact: state.contact.data
    }
}

function mapDispatchToProps(dispatch) {
    return {
        updateContact: (contactInfo, id, props) => dispatch({ type: UPDATE_CONTACT, contactInfo, props, id }),
        setPageTitle: (main, breadcrumb_1, breadcrumb_2) => dispatch(setPageTitle(main, breadcrumb_1, breadcrumb_2)),
        getContact: (id) => dispatch({ type: GET_CONTACT_DETAIL, id })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(UpdateContact));