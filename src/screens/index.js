import Home from "./Home";
import Login from "./Auth/Login";
import Register from "./Auth/Register";
import ResetPassword from "./Auth/ResetPassword";
import ReCharge from "./ReCharge";
import Order from "./Order";
import Contact from "./Contact";
import User from "./User";
import BuffLike from "./BuffLike";
import BuffShare from "./BuffShare";
import BuffLivestream from "./BuffLivestream";
import BuffComment from "./BuffComment";
import UpdateContact from "./Contact/update";
import CreateContact from "./Contact/create";
import VerifyAccount from "./Auth/VerifyAccount";
import VerifyResetPassword from "./Auth/VerifyResetPassword";

export { 
    Home,
    Login,
    Register,
    ResetPassword,
    ReCharge,
    Order,
    Contact,
    User,
    BuffShare,
    BuffLike,
    BuffComment,
    BuffLivestream,
    UpdateContact,
    CreateContact,
    VerifyAccount,
    VerifyResetPassword
};
