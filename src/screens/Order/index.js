import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import { GET_ORDER_ALL } from '../../config/ActionTypes/Order';
import './styleIndex.scss';
import {setPageTitle} from "../../actions/page-title.action";

function PanelTable(props) {

    const [page, setPage] = useState(1);
    const [sizePerPage, setSizePerPage] = useState(10);
    const [sort, setSort] = useState('created_at');
    const [order, setOrder] = useState('desc');
    const allOrder = props.allOrder;

    useEffect(() => {
        props.getAll({
            limit: sizePerPage,
            offset: 0,
            sort: sort,
            order: order,
        });
    }, [])

    function onTableChange(type, newState) {
        props.getAll({
            sort: newState.sortField,
            order: newState.sortOrder,
            limit: newState.sizePerPage,
            offset: (newState.page - 1) * newState.sizePerPage,
        });
        setSort(newState.sortField);
        setOrder(newState.sortOrder);
        setPage(newState.page);
        setSizePerPage(newState.sizePerPage);
    }

    const paginateOptions = {
        page: page,
        sizePerPage: sizePerPage,
        showTotal: true,
        totalSize: allOrder.length != 0 ? allOrder.total : 0,
        hidePageListOnlyOnePage:true,
        paginationTotalRenderer:(from, to, size)=>{
            return(
                <span className="react-bootstrap-table-pagination-total"> Hiển thị từ { from } đến { to } của { size }</span>
            )
        }
    };

    const columns = [{
        dataField: 'object_id',
        text: 'Mã giao dịch',
        sort: true
    }, {
        dataField: 'object_type',
        text: 'Dịch vụ',
        sort: true
    }, {
        dataField: 'number',
        text: 'Số lượng',
        sort: true
    }, {
        dataField: 'price',
        text: 'Tổng tiền',
        formatter: (cell, row, index) => cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
        sort: true
    }];

    if (allOrder.length == 0) {
        return (
            <div style={{ textAlign: "center" }}>
                <p>Bạn chưa có giao dịch nào được thực hiện</p>
            </div>
        )
    } else {
        if (allOrder.total == 0) {
            return (
                <div style={{ textAlign: "center", fontSize: 16 }}>
                    <p>Bạn chưa có giao dịch nào được thực hiện</p>
                </div>
            )
        }
        return (
            <div className="overflow-hidden" style={{ clear: "both" }}>
                <BootstrapTable
                    onTableChange={onTableChange}
                    keyField='_id'
                    remote={true}
                    data={allOrder.length != 0 ? allOrder.rows : []}
                    columns={columns}
                    pagination={paginationFactory(paginateOptions)}
                    keyBoardNav
                />
            </div>
        )
    }
}

function Order(props) {
    useEffect(()=>{
        props.setPageTitle("Đơn hàng của bạn","Đơn hàng của bạn");
    },[])

    return (
        <div id="page-content">
            <div className="row">
                <div className="panel">
                    <div className="panel-body">
                        <div className="row" style={{ padding: 14 }}>
                            <PanelTable {...props} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

function mapStateToProps(state) {
    return {
        allOrder: state.order.allOrder
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getAll: (filters) => dispatch({ type: GET_ORDER_ALL, filters }),
        setPageTitle: (main,breadcrumb_1,breadcrumb_2) =>dispatch(setPageTitle(main,breadcrumb_1,breadcrumb_2))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Order);