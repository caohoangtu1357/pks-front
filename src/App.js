import React, { useEffect } from "react";
import { Redirect, Route, HashRouter as Router, Switch } from "react-router-dom";
import {
  Home,
  Login,
  Register,
  ResetPassword,
  ReCharge,
  Order,
  Contact,
  User,
  BuffShare,
  BuffLike,
  BuffComment,
  BuffLivestream,
  UpdateContact,
  CreateContact,
  VerifyAccount,
  VerifyResetPassword
} from "./screens";

import {
  Header,
  SideBar,
  Footer,
  ScrollTop,
  Loader,
  Alert
} from "./components";

import { connect } from "react-redux";
import { GET_INFO } from "./config/ActionTypes/User";
import { GET_RECHARGE } from "./config/ActionTypes/Recharge";
import "./Style.scss";

function PageHead(props) {
  const title = props.title;
  return (
    <div id="page-head">
      <div id="page-title">
        <h1 className="page-header text-overflow">{title.main}</h1>
      </div>

      <ol className="breadcrumb">
        <li><a href="/"><i className="demo-pli-home"></i></a></li>
        <li>{title.breadcrumb_1}</li>
        {
          title.breadcrumb_2?<li className="active">{title.breadcrumb_2}</li>:""
        }
      </ol>
    </div>
  )
}

function AuthenticatedRoutes(props) {

  useEffect(() => {
    props.getRecharge();
  }, [])

  if (!props.user) {
    return (
      <Redirect to={"login"} />
    )
  }
  return (
    <Router>
      <Switch>
        <Route exact path="/recharge" component={ReCharge} />
        <Route exact path="/order" component={Order} />
        <Route exact path="/contact" component={Contact} />
        <Route exact path="/user" component={User} />
        <Route exact path="/buff-like" component={BuffLike} />
        <Route exact path="/buff-share" component={BuffShare} />
        <Route exact path="/buff-comment" component={BuffComment} />
        <Route exact path="/buff-livestream" component={BuffLivestream} />
        <Route exact path="/contact/update/:id" component={UpdateContact} />
        <Route exact path="/contact/create" component={CreateContact} />
        <Route exact path="/" component={Home} />
        <Route component={Home} />
      </Switch>
    </Router>
  )
}

function AuthRoutes(props) {
  if (props.user) {
    return (
      <Redirect to={"/"} />
    )
  }
  return (
    <Router>
      <Switch>
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={Register} />
        <Route exact path="/reset-password" component={ResetPassword} />
        <Route exact path="/verify-account" component={VerifyAccount} />
        <Route exact path="/verify-reset-password" component={VerifyResetPassword} />
        <Route component={Login} />
      </Switch>
    </Router>
  )
}

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.getInfoUser();
  }

  render() {
    if (this.props.user) {
      return (
        <Router>
          <div id="container" className="effect aside-float aside-bright mainnav-lg navbar-fixed">
            <Alert />
            <Loader />
            <Header />
            <div className="boxed">
              <div id="content-container">
                <PageHead {...this.props}/>
                <AuthenticatedRoutes {...this.props} />
              </div>
              <SideBar />
            </div>
            <Footer />
            <ScrollTop />
          </div>
        </Router>
      );
    } else {
      return (
        <>
          <Alert />
          <AuthRoutes user={this.props.user} />
        </>
      )
    }
  }
}

function mapStateToProps(state) {
  return {
    user: state.user.data,
    title: state.title
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getInfoUser: () => dispatch({ type: GET_INFO }),
    getRecharge: () => dispatch({ type: GET_RECHARGE })
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
