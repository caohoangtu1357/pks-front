import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import createSagaMiddleware from 'redux-saga'
import reducer from './reducer/index';
import axios from 'axios';
import rootSaga from './saga/index';

function changeStyleLoader(display) {
  var elements = document.getElementsByClassName('load1');
  for (var i = 0; i < elements.length; i++) {
    elements[i].style.display = display;
  }
}

axios.interceptors.request.use(request => {
  changeStyleLoader("unset");
  request.headers['Authorization'] = localStorage.getItem("auth-token");
  return request;
},error=>{
  changeStyleLoader("none");
  return Promise.reject(error);
});

axios.interceptors.response.use(response => {
  changeStyleLoader("none");
  return response;
}, error => {
  changeStyleLoader("none");
  return Promise.reject(error);
});

const sagaMiddleware = createSagaMiddleware();
const store = createStore(reducer, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);

const routing = (
  <Provider store={store}>
    <App />
  </Provider>
);

ReactDOM.render(routing, document.getElementById('root'));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
